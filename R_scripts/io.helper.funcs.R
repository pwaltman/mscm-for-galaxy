options( warn=-1 )
try( detach( io.helper.funcs ), silent=T ); rm( io.helper.funcs )
options( warn=0 )

io.helper.funcs <- list(

## Redefine cat() and print() so that they also send output to a log file
cat.new = function( ...,
                    log=get.global( "out.log" ),
                    file="",
                    sep=" ",
                    fill=FALSE,
                    labels=NULL,
                    append=FALSE
                  ) {
  cat( ...,
       file=file,
       sep=sep,
       fill=fill,
       labels=labels,
       append=append
      )
  if ( file == "" && ! is.null( log ) && log != "" )
    try( cat( ..., file=log, append=T ) )
},

print.new = function( x, log=get.global( "out.log" ), ... ) {
  print( x, ... )
  if ( ! is.null( log ) && log != "" )
    try( capture.output( print( x, ... ),
                         file=log,
                         append=T
                        )
        )
},

get.log = function( prefix="biclust.log.%04d.txt",
                    output.dir="output/"
                  ) {
  if ( exists( "out.log" ) )
    return( out.log )
  
  if ( ! file.exists( output.dir ) )
    try( dir.create( output.dir, recursive=T, showWarnings=F ) )
  for ( i in 1:999 ) {
    f <- paste( output.dir,
                sprintf( prefix,
                         as.integer( i )
                        ),
                sep=""
               )
    if ( file.exists( f ) ) next
    f <- paste( getwd(), f, sep="/" )
    out.log <<- f
    cat.new( "Logging all output to", out.log, "\n" )
    break
  }
  return( out.log )
},



get.log.new = function( prefix="biclust.log.%04d.txt",
                        output.dir="output/",
                        clustk=NULL,
                        is.cluster.node=F
                       ) {
  if ( exists( "out.log" ) && is.null( clustk ) )
    return( out.log )
  
  if ( ! file.exists( output.dir ) )
    dir.create( output.dir, recursive=T, showWarnings=F )

  if ( is.cluster.node ) {
    f <- paste( output.dir,
                'initialize.single.snow.node.log.txt',
                sep=""
               )
  }
  else {
    f <- paste( output.dir,
                'head.node.log.txt',
                sep=""
               )
  }

  ##  clustk over-rides is.cluster.node so we have a log
  ##   file for each cluster
  if ( ! is.null( clustk ) ) {
    f <- paste( state.file,
                sprintf( prefix,
                         as.integer( clustk )
                        ),
                sep="."
               )

    if ( exists( "output.dir", ".GlobalEnv" ) ) {
      output.dir <- get.global( "output.dir" )

      if (! file.exists( output.dir ) )
        dir.create( output.dir, recursive=T, showWarnings=F )
    }
    else
      stop( "phw dbg: output.dir not in .GlobalEnv\n" )

    ## delete any old log files
    if ( file.exists( f ) )
      unlink( f )
  }
  
  f <- paste( getwd(), f, sep="/" )
  out.log <<- f
  cat.new( "Logging all output to", out.log, "\n" )

  return( out.log )
}
                        
)

attach( io.helper.funcs )
