## Get the default params and then override some of them here...

stmParams <- function() {

  stm <- list()

  stm$organism <- "stm"
  stm$species <- "Salmonella_typhimurium"
  ##stm$data.types.used  <- c( "expression", "seq.motif" )#, "network" ) # , "network", "seq.motif" )
  stm$is.eukaryotic <- F
  
####################### SINGLE-SPECIES OPT'N PARAMS ############################

  stm$single <- list()

  ##  phw comment: a little redundant, sigh, but I don't know how to make the $single list inherit
  ##               these from the main stm list
  stm$single$organism <- stm$organism
  stm$single$species <- stm$species 
  ##stm$single$data.types.used  <- stm$data.types.used
  stm$single$is.eukaryotic <- stm$is.eukaryotic
  
  stm$single$kmax <- 300
  stm$single$row.ceiling <- 175 ##50 ## 75  stm$ ## 125, 100 ?
  stm$single$max.clust.per.gene <- 3
  stm$single$row.thresh <- 0.5
  stm$single$output.dir <- paste( "output/", stm$organism, "/", sep="" )  
  stm$single$log.dir <- paste( stm$single$output.dir, "logs/", sep="" )

####################### DATA READ-IN FILES ###############################
  stm$read <- list()

  ##  phw comment: a little redundant, sigh, but I don't know how to make the $read list inherit
  ##               these from the main stm list
  stm$read$organism <- stm$organism
  stm$read$species <- stm$species 
  ##stm$read$data.types.used  <- stm$data.types.used
  stm$read$is.eukaryotic <- stm$is.eukaryotic


  stm$read$data.dir <- paste( "data/", stm$organism, "/", sep="" )
#  stm$read$log.dir <- paste( stm$output.dir, "logs/", sep="" )
  stm$read$seqs.fname <- paste( stm$read$data.dir, "/stm.upstream.-200to0.fst.gz", sep="" )
  stm$read$met.net.file <- paste( stm$read$data.dir, "/metUnDir.STM.sif", sep="" )
  stm$read$prolinks.file <- paste( stm$read$data.dir, "prolinks.Salmonella_typhimurium_LT2.txt.gz", sep="" )
  stm$read$gene.coords.file <- c( paste( stm$read$data.dir, "/NC_003197.ptt.gz", sep="" ),
                                  paste( stm$read$data.dir, "/NC_003277.ptt.gz", sep="" )
                                 )
  stm$read$goa.file <- paste( stm$read$data.dir, "/69.S_typhimurium_ATCC_700720.goa.gz", sep="" )

  stm$read$org.code           = "stm"
  stm$read$org                = "S. typhimurium"
  stm$read$chrLen             = 4951371
  stm$read$kegg.pathways.file = paste(stm$read$data.dir, "stm_gene_map.tab", sep="/")
  stm$read$swp.file           = paste(stm$read$data.dir, "stm.swp.txt.gz", sep="/")
  stm$read$kegg.onto          = paste(stm$read$data.dir, "stm00001.keg", sep="/")
  stm$read$annots             = eval({ Q = paste(stm$read$data.dir, "stm.all.gbk.annos", sep="/")
                                         x = as.matrix(read.table(Q, sep="\t", header=T, fill=T, colClasses="character"))
                                         rownames(x) = toupper(x[,4])
                                         x[,5] = toupper(x[,5])
                                         x[,6] = toupper(x[,6])
                                         x })


  
####################### SMD DATA FILES ############################

  stm$read$data.col <- c( "LOG_RAT2N_MEAN", "Log.base2..of.R.G.Normalized.Ratio..Mean." )
  stm$read$gene.col <- c( "Name", "NAME" )
  stm$read$desc.name <- "DESCRIPTION" # Not used yet
  stm$read$short.name <- "GENE.NAME" # Not used yet

  stm$read$valid.gene.regexp <- "^STM[0-9]+$" # Use this to rule out bad spots in SMD data
  stm$read$invalid.condition.regexp = c( "Category=Strain comparison" )

####################### INITIALIZATION ###########################
  
  ##stm$read$nColGroups <- stm$read$kColGroups <- 0
  ##stm$read$seedMode <- "full" ##"rnd"

  stm$read$stdRatios <- TRUE ## FALSE
  stm$read$ratios.thresh <- 0.25 ##1.0
  stm$read$ratios.n.thresh <- 5
  stm$read$ratios.max.nas.perc <- (1/3)
  
####################### MOTIF FINDING #############################

  stm$read$tmp.prefix <- paste( stm$organism, "_Motif_tmp", sep="" )
  stm$iter <- list()
  ##stm$iter$motif.scaling <- c( 0, seq( 0, 0.75, length=30 ), seq( 0.75, 0.25, length=50 ) )
  ##stm$iter$motif.scaling <- c( 0, seq( 0, 0.75, length=30 ), seq( 0.75, 0.50, length=50 ) )
  ##names( stm$iter$motif.scaling ) <- as.character( c( 1, 5:34, 35:84 ) )

  stm$iter$motif.scaling <- seq( 0, 1, length=82 )
  names( stm$iter$motif.scaling ) <- c( 1, seq( 20, 100, by=1 ) )
  
  ##stm$iter$n.motifs <- unlist( list( "1"=1, "25"=2, "60"=3 ) ) ##"35"=2, "50"=3 ) )  
  stm$iter$n.motifs <- unlist( list( "1"=1, "45"=2, "70"=3 ) ) ##"35"=2, "50"=3 ) )

  
#  stm$iter$max.motif.width <- 25
  stm$iter$max.motif.width <- 22
  
####################### NETWORKS ##################################

#  stm$net.max.weights <- unlist( list(operons=2,
#                                         met=0.8,
#                                         prolinks_RS=0.3,
#                                         prolinks_PP=0.4,
#                                         prolinks_GN=0.6,
#                                         prolinks_GC=0.4,
#                                         COG_code=1,
#                                         cond_sims=1,
#                                         predictome_CP=0,
#                                         predictome_GF=0,
#                                         predictome_PP=0.1 ) )
  stm$read$net.max.weights <- unlist( list( operons=0.3,
                                            met=0.8,
                                            prolinks_RS=0.0,
                                            prolinks_PP=0.5,
                                            prolinks_GN=0.0,
                                            prolinks_GC=0.0,
                                            COG_code=1,
                                            cond_sims=0.0,
                                            predictome_CP=0.0,
                                            predictome_GF=0.0,
                                            predictome_PP=0.0
                                           )
                                       )
  
  ##stm$net.weights <- stm$net.weights * 0

  stm$iter$net.scaling <- c( seq( 0.1, 0.3, length=10 ), seq( 0.3, 0.02, length=50 ) )
  ##stm$iter$net.scaling <- c( seq( 0.1, 0.2, length=5 ), seq( 0.2, 0.45, length=60 ) )
  names( stm$iter$net.scaling ) <- as.character( c( 1:10, 11:60 ) )
  ##stm$iter$net.scaling <- 1
  ##names( stm$iter$net.scaling ) <- 1
####################### KILL PARAMS ###############################

  ##stm$kill.count <- 1 # Kill this many clusters each time

####################### BICLUST PARAMS ############################

  ##stm$single <- list()
  ##stm$single$row.ceiling <- 175 ##50 ## 75  stm$ ## 125, 100 ?
  ##stm$single$max.clust.per.gene <- 3
  ##stm$single$row.thresh <- 10
  ##stm$singlex$output.dir <- paste( "output/", stm$organism, "/", sep="" )  
  invisible( stm )
}

stmParams()

