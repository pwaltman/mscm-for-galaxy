# complete validation script for all measures

## libraries
source( 'R_scripts/initial.funcs.R' )
require(ecolitk)
require(GO.db) # tk: do we still need this? and what about kegg?

# source files
source( "R_scripts/validation/valUtils2.R" )

# tk: this is assuming clusterStack, params, validate() ... are all in the same environment
## biclust.version should be in params$common
validate <- function( clusterStack=get.global( 'clusterStack' ),
                      mode="elaborate",
                      val.todo=NULL
                     ) {

  if (!is.element( mode, c( "elaborate", "single", "shared" ) ) ) 
    stop("mode: ", mode, " not supported (only elaborate, single or shared)\n")		

  if ( mode=='single' ) {
    initialize.single.cMonkey()
    if ( all( names( clusterStack ) %in% c( "", "k" ) ) ) {
      organisms <<- get.global( 'organism' )
      bak <- clusterStack
      clusterStack <- list(); clusterStack[[ organisms[[1]] ]] <- bak; rm( bak )
    }
  }
  if ( mode %in% c( 'elaborate', 'shared' ) ) {
    initialize.shared.cMonkey()
  }

  ## if we are running from scratch, get the organisms from the clusterStack
  ## can be one or more organisms
  if ( mode=='single' && all( names( clusterStack ) %in% c( "", "k" ) ) ) {
    ## if we're working w/an old format cstack (just the bicl's, not cstack[[org]] format)
    ##  get the organisms from the global organism var (should be available)
    organisms <<- get.global( 'organism' )
    bak <- clusterStack
    clusterStack <<- list(); clusterStack[[ organisms[[1]] ]] <<- bak; rm( bak )
    
  }
  else {
    ## in case the shared clusterStack hasn't been separated into org-specific sub-lists
    if ( is.null( names( clusterStack ) ) ) {
      bak <- clusterStack
      clusterStack <<- list()
      for ( org in organisms ) {
        clusterStack[[ org ]] <<- lapply( bak, '[[', org )
      }
      rm( bak )
      clusterStack <- get.global( 'clusterStack' )
    }
  }
  ## the validations to do could depend on what data types are used
  ## or could be chosen manually for testing or otherwise:
  if ( is.null( val.todo ) )
    val.todo <- do.validation()
  ## for now...
  ##val.todo = "all"

  ## put all the machinery into the same loop
  for ( org in organisms ) {

    olog.pairs <- NULL
    if ( mode == 'shared' )
      olog.pairs <- pairs.from.db( org=organisms )

    vp <- getValidationParams( org, params, olog.pairs )

    if ( ! file.exists( vp$val.out.dir ) )
      dir.create( vp$val.out.dir, recursive=TRUE ) 

    vp$val.log <- paste( vp$val.out.dir,
                         paste( vp$organism, "validation.log.txt", sep="." ),
                         sep="/" )
 	
    cat.new( "Validation: ", date(), "\n",
             "for run: ", date.biclust.run[1], "\n",
             "organisms: ", organisms, "\n",
             "valdations to do:\n", paste( val.todo, collapse="\n " ), "\n",
             "\n", toupper(org), "\n\n", 
             "mode: ", mode, "\n",
             log = vp$val.log )

    ## the clusterStack should have the same structure
    ## for any mode, any number of species
    ##cs <- list()
    if ( all( organisms %in% names( clusterStack ) ) ) {
      cs <- clusterStack[[ org ]]
    }
    else {
      cs <- lapply( clusterStack, '[[', org )
    }
    cs$k <- length(cs)

    if ( mode == "shared" ){
      vp$n.genes <- vp$n.ologs
      vp$ologs <- vp$org.data$gene_name[ unique( olog.pairs[[ org ]] ) ]
    }
    ## convert ids to gene names
    for ( i in 1:cs$k )
      {
        cs[[ i ]]$orf <- vp$org.data$gene_name[ cs[[i]]$rows ]
        cs[[ i ]]$rows <- vp$org.data$gene_name[ cs[[i]]$rows ]
        cs[[ i ]]$conds <- vp$org.data$cond_name[ cs[[ i ]]$conds ]
      }

    vp$org.cls <- cs

    cat.new( "number of biclusters: ", cs$k, "\n\n", log=vp$val.log, append=T)
		
######################################################################################################

    ## compute coverage and overlap
    if ( "coverage" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/coverage5.R")
      coverage( vp, mode )
      test.overlap( vp )
    }				 
  
######################################################################################################	

    ## compute COG enrichment
    if ( "COG enrichment" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/cog.validation1.R")
      cog.enrichment( vp, mode )
    }
 
######################################################################################################	

    ## compute internal measures
    if ( "internal measures" %in% val.todo || "all" %in% val.todo ) {
      #save( file='vp.dbg.rda', vp ); stop( 'phw dbg\n' )
      source("R_scripts/validation/internal.measures4.R")
      internal.measures( vp )
    }

######################################################################################################

    ## compute motif comparisons
    if ( "compare motifs" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/compare.motifs6.R")
      compare.motifs( vp, nuc.overlap=6)
    }

######################################################################################################

    ## compute KEGG enrichment
    if ( "KEGG enrichment" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/kegg.validation6.R")
      kegg.enrichment( vP=vp, mode=mode )
    }
    
######################################################################################################

    ## compute GO enrichment
    if ( "GO enrichment" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/go.validation7.R")
      go.enrichment( vP=vp,
                     clusterStack=clusterStack,
                     mode=mode )
    }

######################################################################################################

    ## chromosome maps
    if ( "chromosome location maps" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/chromosome.location.map3.R")
      map.locations( vp )
    }

######################################################################################################
  }

  cat.new( "validation finished", date(), "\n", log=vp$val.log, append=T )
} # end validate


validate.individuals <- function() {

  if ( ! exists( "organisms", ".GlobalEnv" ) )
    organisms <- get.organisms()
  else
    organisms <- get.global( 'organisms' )
  
  ## the validations to do could depend on what data types are used
  ## or could be chosen manually for testing or otherwise:
  ##val.todo <- do.validation()
  ## for now...
  val.todo = "all"
	
  ## put all the machinery into the same loop
  for ( org in organisms ) {

    organism <<- org
    initialize.single.cMonkey()
    dataDir <- "data"
    resultsDir <- "individual.species.results"
		
    ## get and load clusterStack
    files <- list.files( paste( resultsDir, org, sep="/" ), pattern="final.clusters.RData")
    files.path = list.files( paste( resultsDir, org, sep="/" ), pattern="final.clusters.RData", full=T)
    if ( length( files.path ) == 1 )
      load( files.path )
    else 
      load( switch( menu( files ), files.path ) )

    ## get and load global.data
    files <- list.files( paste( resultsDir, org, sep="/" ),
                         pattern="global.data.RData")
    
    files.path <- list.files( paste( resultsDir, org, sep="/" ),
                              pattern="global.data.RData", full=T)
    if ( length( files.path ) == 1 )
      load( files.path )
    else
      load( switch( menu( files ), files.path ) )
		
    #vp <- getVersion1Params( org, dataDir )
    ologs <- list(); ologs[[ organism ]] <- list()
    params <- get.global( 'params' )
    vp <- getValidationParams( org, params, ologs=list( organism=list())	 )

    cat( 'dbg:', vp$val.out.dir, "\n" )
    
    if (! file.exists( vp$val.out.dir ) )
      dir.create( vp$val.out.dir, recursive=TRUE ) 

    vp$val.log <- paste( vp$val.out.dir, paste( vp$organism, "validation.log.txt", sep="." ),  sep="/" ) 
 	
    cat.new( "Validation: ", Sys.Date(), "\n",
             "organisms: ", organisms, "\n",
             "val to do: ", val.todo, "\n",
             "\n", toupper(org), "\n\n", 
             log=vp$val.log )		
    ## for now. should put this somewhere else later (function of something)
    ## really fix this soon.
    if (org == "bsubt" ) {
      load("individual.species.results/bsubt/gene.name.to.bsu.translations.RData")
      gene_name <- gene.name.to.bsu[! is.na(gene.name.to.bsu) ]
      vp$n.genes <- length(gene_name)

      for ( i in 1:clusterStack$k )
      	{
          clusterStack[[ i ]]$conds <- clusterStack[[ i ]]$cols
          clusterStack[[ i ]]$nconds <- clusterStack[[ i ]]$ncols
          clusterStack[[ i ]]$genes <- clusterStack[[ i ]]$rows
          genes <- as.vector( gene_name[ clusterStack[[ i ]]$rows ] )
          clusterStack[[ i ]]$rows <- genes[ ! is.na( genes ) ]
      	}

    }
    else {
      vp$n.genes <- global.data$n.genes.tot
      for ( i in 1:clusterStack$k )
      	{
          clusterStack[[ i ]]$conds <- clusterStack[[ i ]]$cols
          clusterStack[[ i ]]$nconds <- clusterStack[[ i ]]$ncols
          clusterStack[[ i ]]$genes <- clusterStack[[ i ]]$rows
      	}
      gene_name <- names(gene.coords$gene.name)
    }

    vp$n.conds <- global.data$n.conds

    vp$org.cls <- clusterStack    
    names(gene_name) <- NULL
    vp$org.data <- list( gene_name=gene_name,
                         gene_code=gene.coords$gene.code,
                         cond_name=colnames(global.data$ratios)
                        )
		
    cat.new( "number of biclusters: ", vp$org.cls$k, "\n",
             log=vp$val.log )
    cat.new( "validating:", org, "\n",
             log=vp$val.log )
		
######################################################################################################

    ## compute coverage and overlap
    if ( "coverage" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/coverage5.R")
      coverage( vp, mode='single' )
      test.overlap( vp )
    }				 
  
######################################################################################################	

    ## compute COG enrichment
    if ( "COG enrichment" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/cog.validation1.R")
      cog.enrichment( vp, mode='single' )
    }
 
######################################################################################################	

    ## compute internal measures
    if ( "internal measures" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/internal.measures4.R")
      internal.measures( vp )
    }

######################################################################################################

    ## chromosome maps
    if ( "chromosome location maps" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/chromosome.location.map3.R")
      map.locations( vp )
    }
######################################################################################################	

    ## compute motif comparisons
    if ( "compare motifs" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/compare.motifs6.R")
      compare.motifs( vp, nuc.overlap=6)
    }

######################################################################################################

    ## compute KEGG enrichment
    if ( "KEGG enrichment" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/kegg.validation6.R")
      kegg.enrichment( vp )
    }

######################################################################################################

    ## compute GO enrichment
    if ( "GO enrichment" %in% val.todo || "all" %in% val.todo ) {
      source("R_scripts/validation/go.validation7.R")
      go.enrichment( vp )
    }

######################################################################################################
  }
  cat.new( "validation finished", date(), "\n", log=vp$val.log, append=T )
} # end validate.individuals

