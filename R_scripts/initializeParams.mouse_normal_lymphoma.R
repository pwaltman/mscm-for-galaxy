## Get the default params and then override some of them here...

mouseParams <- function() {

  mouse_normal_lymphoma <- list()

  mouse_normal_lymphoma$organism <- "mouse_normal_lymphoma"
  mouse_normal_lymphoma$species <- "Mus musculus"
  mouse_normal_lymphoma$data.types.used  <- c( "expression", "network" ) # , "network", "seq.motif" )
  mouse_normal_lymphoma$is.eukaryotic <- T
  
####################### SINGLE-SPECIES OPT'N PARAMS ############################

  ##mouse_normal_lymphoma$single <- list()
  ##  phw comment: a little redundant, sigh, but I don't know how to make the $single list inherit
  ##               these from the main mouse_normal_lymphoma list
  mouse_normal_lymphoma$single$organism <- mouse_normal_lymphoma$organism
  mouse_normal_lymphoma$single$species <- mouse_normal_lymphoma$species 
  mouse_normal_lymphoma$single$data.types.used  <- mouse_normal_lymphoma$data.types.used
  mouse_normal_lymphoma$single$is.eukaryotic <- mouse_normal_lymphoma$is.eukaryotic
  
  mouse_normal_lymphoma$single$kmax <- 700
  mouse_normal_lymphoma$single$row.ceiling <- 175 ##50 ## 75  mouse_normal_lymphoma$ ## 125, 100 ?
  mouse_normal_lymphoma$single$max.clust.per.gene <- 3
  mouse_normal_lymphoma$single$row.thresh <- 0.5
  mouse_normal_lymphoma$single$output.dir <- paste( "output/", mouse_normal_lymphoma$single$organism, "/", sep="" )
  mouse_normal_lymphoma$single$log.dir <- paste( mouse_normal_lymphoma$single$output.dir, "logs/", sep="" )
  
####################### DATA READ-IN FILES ###############################

  mouse_normal_lymphoma$read <- list()

  ##  phw comment: a little redundant, sigh, but I don't know how to make the $read list inherit
  ##               these from the main mouse list
  mouse_normal_lymphoma$read$organism <- mouse_normal_lymphoma$organism
  mouse_normal_lymphoma$read$species <- mouse_normal_lymphoma$species 
  mouse_normal_lymphoma$read$data.types.used  <- mouse_normal_lymphoma$data.types.used
  mouse_normal_lymphoma$read$is.eukaryotic <- mouse_normal_lymphoma$is.eukaryotic

  
  mouse_normal_lymphoma$read$data.dir <- paste( "data/", mouse_normal_lymphoma$organism, "/", sep="" )
  mouse_normal_lymphoma$read$log.dir <- paste( mouse_normal_lymphoma$read$output.dir, "logs/", sep="" )
  mouse_normal_lymphoma$read$ratios.fname <- paste( mouse_normal_lymphoma$read$data.dir, "ratios.table.txt.gz", sep="" )
  mouse_normal_lymphoma$read$seqs.fname <- paste( mouse_normal_lymphoma$read$data.dir, "mouse.upstream.3kb.fna.gz", sep="" )
  mouse_normal_lymphoma$read$gene.coords.file <- paste( mouse_normal_lymphoma$read$data.dir, "/mouse.ptt", sep="" )

  ## new 05.26.2011
  mouse_normal_lymphoma$read$curated.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.curated.sif", sep="" )
  mouse_normal_lymphoma$read$inferred.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.inferred.sif", sep="" )
  
  
  ## these are old, but kept for time being until we decide whether we want to keep
  ##   the approach of using combined networks 
  mouse_normal_lymphoma$read$bind.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.bind.sif", sep="" )
  mouse_normal_lymphoma$read$biogrid.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.biogrid.sif", sep="" )
  mouse_normal_lymphoma$read$dip.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.dip.sif", sep="" )
  mouse_normal_lymphoma$read$hprd.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.hprd.apid.sif", sep="" )
  mouse_normal_lymphoma$read$innatedb.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.innatedb.sif", sep="" )
  mouse_normal_lymphoma$read$intact.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.intact.sif", sep="" )
  mouse_normal_lymphoma$read$interoporc.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.interoporc.sif", sep="" )
  mouse_normal_lymphoma$read$matrixdb.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.matrixdb.sif", sep="" )
  mouse_normal_lymphoma$read$mint.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.mint.sif", sep="" )
  mouse_normal_lymphoma$read$string.sif <- paste( mouse_normal_lymphoma$read$data.dir, "mm.string.sif", sep="" )


  ###  these are old network files that we don't use with mouse & human anymore
  ###   we just comment them out for the moment, but they probably should be removed eventually
  #mouse_normal_lymphoma$read$met.net.file  <- NULL
  #mouse_normal_lymphoma$read$pp.interaction.sif <- NULL
  #mouse_normal_lymphoma$read$tf.list.file <- NULL 
  #mouse_normal_lymphoma$read$prolinks.file <- NULL
  #mouse_normal_lymphoma$read$pd.interaction.sif <- NULL
  mouse_normal_lymphoma$read$goa.file <- paste( mouse_normal_lymphoma$read$data.dir, "/gene_association.goa_mouse.gz", sep="" )  #NULL
  mouse_normal_lymphoma$read$swp.file <- NULL
  mouse_normal_lymphoma$read$kegg.pathways.file <- NULL
  #mouse_normal_lymphoma$read$seed.file <- paste( mouse_normal_lymphoma$read$data.dir, "seed_set_050408.txt", sep="" )
  #mouse_normal_lymphoma$read$seed.file <- paste( mouse_normal_lymphoma$read$data.dir, "LinSeed.txt", sep="" )
  #mouse_normal_lymphoma$read$sample.seed <- T
  
  mouse_normal_lymphoma$read$predictome.code <- "mouse"              ## Code for species
  
####################### SMD DATA FILES ############################

  mouse_normal_lymphoma$read$data.col <- c( "LOG_RAT2N_MEAN", "Log.base2..of.R.G.Normalized.Ratio..Mean." )
  mouse_normal_lymphoma$read$gene.col <- c( "Name", "NAME" )
  mouse_normal_lymphoma$read$desc.name <- "DESCRIPTION" # Not used yet
  mouse_normal_lymphoma$read$short.name <- "GENE.NAME" # Not used yet

  mouse_normal_lymphoma$read$valid.gene.regexp <- "^GBAA.+$" # Use this to rule out bad spots in SMD data
  mouse_normal_lymphoma$read$invalid.condition.regexp = c( "Category=Strain comparison" )

####################### INITIALIZATION ###########################
  
  ##mouse_normal_lymphoma$read$nColGroups <- mouse_normal_lymphoma$read$kColGroups <- 0
  ##mouse_normal_lymphoma$read$seedMode <- "full" ##"rnd"

  mouse_normal_lymphoma$read$stdRatios <- TRUE ## FALSE
  mouse_normal_lymphoma$read$ratios.thresh <- 0.25 ##1.0
  mouse_normal_lymphoma$read$ratios.n.thresh <- 5
  mouse_normal_lymphoma$read$ratios.max.nas.perc <- (1/3)
  
####################### MOTIF FINDING #############################

  mouse_normal_lymphoma$read$tmp.prefix <- paste( mouse_normal_lymphoma$organism, "_Motif_tmp", sep="" )
  ##mouse_normal_lymphoma$iter <- list()
#  mouse_normal_lymphoma$iter$motif.scaling <- c( 0, seq( 0, 0.75, length=30 ), seq( 0.75, 0.25, length=50 ) )
#  names( mouse_normal_lymphoma$iter$motif.scaling ) <- as.character( c( 1, 5:34, 35:84 ) )
  #mouse_normal_lymphoma$iter$motif.scaling <- c( 0, seq( 0, 0.75, length=30 ), seq( 0.75, 0.50, length=50 ) )
  
#  mouse_normal_lymphoma$iter$n.motifs <- unlist( list( "1"=1, "25"=2, "40"=3 ) ) ##"35"=2, "50"=3 ) )
  #mouse_normal_lymphoma$iter$n.motifs <- unlist( list( "1"=1, "25"=2, "60"=3 ) ) ##"35"=2, "50"=3 ) )
  
  #mouse_normal_lymphoma$iter$max.motif.width <- 25
  #mouse_normal_lymphoma$iter$max.motif.width <- 20
  
####################### NETWORKS ##################################

  mouse_normal_lymphoma$read$net.max.weights <- unlist( list( curated=0.5,
                                                              inferred=0.005 ) )
  
  mouse_normal_lymphoma$iter$net.scaling <- c( seq( 0.1, 0.3, length=10 ), seq( 0.3, 0.02, length=50 ) )
  names( mouse_normal_lymphoma$iter$net.scaling ) <- as.character( c( 1:60 ) )

  #mouse_normal_lymphoma$iter$net.scaling <- c( seq( 0.1, 0.2, length=5 ), seq( 0.2, 0.02, length=50 ) )
  #mouse_normal_lymphoma$iter$net.scaling <- c( seq( 0.1, 0.2, length=5 ), seq( 0.2, 0.45, length=60 ) )
  
####################### KILL PARAMS ###############################

  ##mouse_normal_lymphoma$kill.count <- 1 # Kill this many clusters each time

  invisible( mouse_normal_lymphoma )
}

mouseParams()
