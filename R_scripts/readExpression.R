## Sept. 2004   DJR
##
## readExpression.R - Read expression matrix and lambdas
##
##
############################## 
# READ in parsed SMD marray files
##############################

#### TOU REALLY WANT MARRAY.FILE TO EXIST!!!

readExpression <- function( marray.file=paste( data.dir, "/ratios.table.txt.gz", sep="" ),
                            check.failed=F ) { 

  lambdas <- NULL

  if ( ! file.exists( marray.file ) ) {

    all.genes <- character()
    rats.list <- list()

    files <- list.files( paste( data.dir, "/marray", sep="" ) )
    files <- files[ grep( ".xls.gz", files ) ]

    ind <- 0

    invalid.fnames <- character( 0 )
    if ( exists( "invalid.filenames" ) )
      invalid.fnames <- paste( invalid.filenames, ".xls.gz", sep="" )
	
    for ( file in files ) {
      ind <- ind + 1

      if ( file %in% invalid.fnames ) {
        cat.new( "Skipping: invalid filename:", ind, file, "\n=================================\n" )
        next
      }
		
      gzfname <- paste( data.dir, "/marray/", file, sep="" )
      cat.new( "Reading in marray data file", ind, "of", length( files ), ":", gzfname, "\n" )
      system( paste( "gunzip", gzfname ) )

      tmp <- strsplit( gzfname, "\\." )[[ 1 ]]
      fname <- paste( tmp[ -length( tmp ) ], sep=".", collapse="." )
		
      zz <- file( fname, "r" )
      y <- readLines( zz, n=100 )

      if ( exists( "invalid.condition.regexp" ) ) {
        found <- F
        for ( regexp in invalid.condition.regexp ) {
          if ( length( grep( regexp, y ) > 0 ) ) {
            cat.new( "Skipping:", regexp, "\n=================================\n" )
            found <- T
            break
          }
        }
        if ( found ) {
          close( zz )
          system( paste( "gzip", fname ) )
          next
        }
      }
		
      skip <- grep( "SPOT", y )[ 1 ] - 1
      if ( is.na( skip ) )
        skip <- grep( "Spot", y )[ 1 ] - 1
      expt <- y[ grep( "!Experiment Name=", y )[ 1 ] ]
      expt <- strsplit( expt, "\\=" )[[ 1 ]][ 2 ]
      cat.new( "Experiment =", expt, "\n" )
      cat.new( y[ grep( "!Category=", y )[ 1 ] ], "\n" )
      close( zz )
		
      zz <- file( fname, "r" )
      marr.data <- read.table( zz, header=TRUE, sep="\t", quote="", skip=skip, comment.char="", as.is=T )
      close( zz )

      system( paste( "gzip", fname ) )

      if ( ! any( gene.col %in% names( marr.data ) ) )
        cat.new( "ERROR: Cannot find gene name column", gene.col, "\n" )
      g.cols <- gene.col[ which( gene.col %in% names( marr.data ) ) ]

      n.valids <- numeric()
      for ( g.col in g.cols ) {
        genes <- as.vector( marr.data[[ g.col ]] )
        n.valids[ g.col ] <- length( grep( valid.gene.regexp, genes, perl=T ) )
      }
      g.col = g.cols[ which.max( n.valids ) ]
      cat.new( "Gene name column:", g.col, "; " )
      genes <- as.vector( marr.data[[ g.col ]] )
      if ( max( n.valids ) < 1000 )
        cat.new( "WARNING: TOO FEW VALID GENE NAMES! (", max(n.valids), ")\n" )
      
      if ( ! any( data.col %in% names( marr.data ) ) )
        cat.new( "ERROR: Cannot find data column", data.col, "\n" )
      d.col <- data.col[ which( data.col %in% names( marr.data ) ) ][ 1 ]
      vals <- marr.data[[ d.col ]]
      cat.new( "Data column:", d.col, "\n" )
      
      ## this failed val seemes to have inconsistent meaning across experiments ... 
      ## not reliable to throw out data that has failed  == T
      failed <- rep( FALSE, length( vals ) )
      if (check.failed) {
        if ( "FAILED" %in% names( marr.data ) )
          failed <- as.numeric( marr.data$FAILED ) != 0
      }

      u.genes <- unique( genes )
      u.genes <- u.genes[ grep( valid.gene.regexp, u.genes, perl=T ) ]
      all.genes <- unique( c( all.genes, u.genes ) )
      g.vals <- numeric()
		
      ##valid <- substr( u.genes, 1, nchar( valid.gene.start ) ) == valid.gene.start
      ##valid <- 1:length( u.genes ) %in% grep( valid.gene.regexp, u.genes, perl=T )
      ##names( valid ) <- u.genes
      ##if ( sum( valid ) < 1000 ) cat.new( "WARNING: TOO MANY INVALID GENE NAMES!\n" )
      if ( length( u.genes ) < 1000 )
        cat.new( "WARNING: TOO MANY INVALID GENE NAMES!\n" )
      
      cat.new( length( genes ), "measurements; ", length( u.genes ), "valid unique genes; " )
      ##rm( valid ) ## RB
      
      reps <- 0
      invalid <- 0
      for ( g in u.genes ) {
        if ( g == "" || is.na( g ) || is.null( g ) )
          next
        ##if ( g %in% names( g.vals ) ) next
        ##if ( valid.gene.start != "" && substr( g, 1, nchar( valid.gene.start ) ) != valid.gene.start ) next
        ##if ( ! valid[ g ] ) next
        whch <- which( genes == g & ! failed )
        if ( length( whch ) > 1 ) {
          g.vals[ g ] <- mean( vals[ whch ], na.rm=T )
          ##cat.new("HERE:",g,whch,"|",vals[whch],"|",g.vals[g],"\n")
          reps <- reps + 1
        } else if ( length( whch ) == 1 ) {
          g.vals[ g ] <- vals[ whch ]
        } else if ( length( whch ) <= 0 ) {
          g.vals[ g ] <- NA
          invalid <- invalid + 1
        }
      }
      cat.new( reps, "are replicated;", invalid, "are invalid\n" )
      
      orig.expt <- expt
      expt.num <- 1
      while ( expt %in% names( rats.list ) ) {
        expt.num <- expt.num + 1
        expt <- paste( orig.expt, ".", expt.num, sep="" )
      }
      if ( expt != orig.expt )
        cat.new( "Renaming to", expt, "\n" )
      
      rats.list[[ expt ]] <- g.vals
      ##if ( plot.hists ) {
      ##  hist( g.vals, breaks=100, main=file, sub=expt )
      ##  cat.new( "Mean =", mean( g.vals, na.rm=T ), "+/-", sd( g.vals, na.rm=T ), "\n" )
      ##  cat.new( "Median =", median( g.vals, na.rm=T ), "\n" )
      ##}
      
      cat.new( "=================================\n" )
      ##if ( file == files[ 5 ] ) break
      gc()
    }
    
    ##if ( plot.hists ) dev.off()
    
    all.genes <- sort( all.genes )
    ratios <- matrix( nrow=length( all.genes ), ncol=length( rats.list ) )
    rownames( ratios ) <- toupper( all.genes )
    colnames( ratios ) <- names( rats.list )
    c.names <- names( rats.list ) ##c( "GENE", names( rats.list ) )
	
    for ( i in names( rats.list ) ) {
      tmp <- rats.list[[ i ]][ all.genes ]
      names( tmp ) <- all.genes
      ratios[ , i ] <- tmp
    }
    
    ##ratios <- strip.na.col( ratios, max.na.frac = 0.30 )
    cat.new("dim ratios: ", dim( ratios ), "\n" )
    ##return( ratios )
    
    ##save( ratios, file="data/ratios.RData", compress=T )
    write.table( ratios,
                 file=paste( data.dir, "/ratios.table.txt", sep="" ),
                 sep="\t",
                 row.names=all.genes,
                 col.names=c.names )
    system( paste( "gzip -v ", data.dir, "/ratios.table.txt", sep="" ) )

    cat.new( "Got", ncol( ratios ), "valid conditions.\n" )
    ##rm( all.genes, rats.list ) ## free up memory
    
  } 
  
  if ( file.exists( marray.file ) ) {
    
    cat.new( "Reading ratios from", marray.file, "\n" )
    
    if ( is.eukaryotic && ( organism != "yeast" ) )
      ratios <- read.large.ratios( marray.file, add.dimnames=T )
    else
      ratios <- as.matrix( read.table( file=gzfile( marray.file ), sep="\t", as.is=T, header=T ) )
    ## ratios <- ratios[ -1, ] # tk: removed because no 16srna in file
    
    if ( exists( "ratios.max.nas.perc" ) ){
      
      cat.new( "filtering the ratios table for rows/conditions with too many NA's\n")
      cat.new( "starting dimensions:", dim( ratios ), "\n" )
      nconds <- ncol( ratios )
      nrows <- nrow( ratios )
      
      ## filter out the genes which have too many NA's in them    
      sum.gene.nas <- sapply( 1:nrows, function(i) sum( is.na( ratios[i,] ) ) )
      ##ratios <- ratios[ which( sum.gene.nas < (nconds*ratios.max.nas.perc) ), ]

      ## now do the same thing for the conds
      #sum.cond.nas <- sapply( 1:nconds, function(i) sum( is.na( ratios[,i] ) ) )
      ##ratios <- ratios[ , which( sum.cond.nas < (nrows*ratios.max.nas.perc) ) ]
      cat.new( "final dimensions:", dim( ratios ), "\n" )
    
    }
  
  
  }
  else 
    cat.new( "ERROR: NO MICROARRAY DATA!!!\n" )
  
  invisible( list( ratios=ratios, lambdas=lambdas, knockIn=list(), knockIn.names=character() ) )
}

strip.na.col <- function( ratios, max.na.frac = 0.10 ) {
	
  max.na <- dim(ratios)[1] * max.na.frac
  exclu <- integer()
  for ( j in 1:(dim(ratios)[2]) ) {
    ##cat.new("adding ", j ," to col eclu\n")
    if ( sum(is.na(ratios[,j])) >= max.na ) exclu <- c( exclu, j)
  }
  if ( length(exclu) > 0) {
    cat.new("exclu:", exclu, "\n")
    return( ratios[, - exclu ])
  } else {
    return( ratios )
  }
}

strip.na.row <- function( ratios, max.na.frac = 0.10 ) {

  max.na <- dim(ratios)[2] * max.na.frac
  exclu <- integer()
  for ( i in 1:(dim(ratios)[1]) ) {
    ##cat.new("adding ", i ," to row eclu\n")
    if ( sum(is.na(ratios[i,])) >= max.na ) exclu <- c( exclu, i)
  }
  if ( length(exclu) > 0) {
    cat.new("exclu:", exclu, "\n")
    return( ratios[ - exclu , ])
  } else {
    return( ratios )
  }
}

## omit.col <- c(534,535,536,537)
## ratios <- as.matrix(test.data[ , - omit.col] )

knockOut <- function( rows, cols, knockIn, knockIn.names, mode = "index") {
													# mode => "names" or "index"
knock.vec <- list()
n <- 0
if (mode == "index") {
	i <- 0
	for (row in rows) {
		i <- i +1
		if (row %in% knockIn.names) {
		knock.vec[[i]] <- which ( cols %in% knockIn[[row]] )
		if (length(knock.vec[[i]]) > 0) {
			n <- n + 1
		} else {
			knock.vec[[i]] <- 0
		}
		} else {
		knock.vec[[i]] <- 0
		}
	}

} else {
	i <- 0
	for (row in rows) {
		i <- i +1
		if (row %in% knockIn.names) {
		knock.vec[[i]] <- knockIn[[row]]
		if (length(knock.vec[[i]]) > 0) {
			if (length(knock.vec[[i]]) > 0) {
				n <- n + 1
			} else {
				knock.vec[[i]] <- 0
			}
		}
		} else {
		knock.vec[[i]] <- 0
		}
	}
}
knock.vec$numKn <- n
return (knock.vec)
}



read.large.ratios <- function( marray.file,
                               delim="\\t",
                               header=T,
                               add.dimnames=F,
                               grab=10^4) {

  if ( ! file.exists( marray.file ) )
    stop( "Err in read.large.ratios:", marray.file, "can't be found\n" )

  #n.rows <- as.numeric( system( paste( "zcat", marray.file, "| wc -l" ), intern=T ) )
  n.rows <- length( readLines( gzfile( marray.file ) ) ) - 1
  tmp <- read.delim( gzfile( marray.file ), nrows = 1, row.names=1 )
  my.cols <- colnames( tmp )
  my.rows <- character()
  n.cols <- length( my.cols )
  m <- matrix( 0, nr=n.rows, nc=n.cols )
  
  my.skip <- 0
  my.nrows <- grab

  tryRes <- try( tmp <- as.matrix( read.delim( gzfile( marray.file ),
                                               skip=my.skip,
                                               nrows=my.nrows,
                                               row.names=1
                                              )
                                  ),
                 silent=T
                )

  if ( class( tryRes ) == "try-error" ) {
    save( file="dbg.RData", list=ls() )
    stop( "phw dbg -- first read fails\n" )
  }
  colnames( tmp ) <- NULL
  my.rows <- c( my.rows, rownames( tmp ) )

  ## have to do this b/c of differences in how read.delim & read.table
  ##  interpret the skip arg
  my.skip <- my.skip+1  

  ## sometimes the format of the file causes the values to be read as chars instead of numerics
  ##  so, check the format and fix if necesary
  if ( class( tmp[,2] ) == 'character' )
    tmp <- matrix( as.numeric( tmp ), nr=nrow( tmp ) )
  
  while( ( (my.skip-1)+nrow( tmp )) <= n.rows) {
    
    cat( "filling",my.skip, "to", my.skip+nrow(tmp)-1, "\n" )
#    save( file="dbg.RData", list=ls() )
    m[ ( my.skip ):( (my.skip-1)+nrow(tmp) ), ] <- tmp

    my.skip <- my.skip + my.nrows
    if ( my.skip > n.rows )
      next
    
    ##  we don't need to update my.nrows to 'fit' what's left b/c
    ##  read.table will only return what's left in the file, even if it's asked
    ##  to read more lines than than are in the file
    ##my.nrows <- min( grab, ( n.rows-( my.skip-1) ) )
    ##browser()
    
    tryRes <- try( tmp <- read.table( gzfile( marray.file ),
                                      skip=my.skip,
                                      nrows=my.nrows,
                                      fill=T,
                                      col.names=c( 1:(n.cols + 1 ) )
                                     ),
                  silent=T
                  )
    if( class( tryRes ) == "try-error" ) 
      stop( paste( "error while reading lines",
                   my.skip, "to", my.skip+my.nrows,
                   "on 2nd try. stopping...\n" )
           )

    ## it worked, so it means that some of the cols were read as factors instead of numerics
    ## so, try to fix those
    rval <- character()
    for ( i in 2:ncol(tmp) )
      rval <- c( rval, class(tmp[,i]) )

    bad.cols <- which( rval %in% 'factor' ); bad.cols <- bad.cols+1
    for ( i in bad.cols )
      tmp[, i] <- as.numeric( as.character( tmp[,i] ) )
      

    my.rows <- c( my.rows, as.character( tmp[,1] ) )
    ##browser()
    tmp <- as.matrix( tmp[,-1] )
    colnames( tmp ) <- NULL

  }


  if ( add.dimnames ) {
    rownames( m ) <- my.rows
    colnames( m ) <- my.cols
    return( m )
  }

  return( list( ratios=m,
                genes=my.rows,
                conds=my.cols
               )
         )
                                                
}
