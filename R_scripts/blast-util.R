blast.util <- list( 

blast.align = function( seqs1, seqs2, addl.params="", full.out=F ) {
  file1 <- tempfile( "blast." )
  file2 <- tempfile( "blast." )
  if ( length( seqs1 ) == 1 && length( seqs2 ) == 1 ) {
    cat( ">seq1\n", seqs1, "\n", sep="", file=file1 )
    cat( ">seq2\n", seqs2, "\n", sep="", file=file2 )
    cmd <- paste( bl2seq.cmd, "-i", file1, "-j", file2, "-p blastn", addl.params )
    if ( ! full.out ) cmd <- paste( cmd, "-D 1" )
    cat.new( cmd, "\n" )
    output <- system( cmd, intern=TRUE, ignore=TRUE )
  }
  else {
    if ( length( seqs1 ) == 1 )
      cat( ">seq1\n", seqs1, "\n", sep="", file=file1 )
    else
      cat( paste( ">", names( seqs1 ), "\n", seqs1, sep="" ), file=file1, sep="\n" )
    if ( length( seqs2 ) == 1 )
      cat( ">seq2\n", seqs2, "\n", sep="", file=file2 )
    else
      cat( paste( ">", names( seqs2 ), "\n", seqs2, sep="" ), file=file2, sep="\n" )
    tmp.log.file <- tempfile( "formatdb.log" )

    if ( length( seqs1 ) >= length( seqs2 ) ) {
      cmd <- paste( formatdb.cmd, "-l", tmp.log.file, "-i", file1, "-p F -o T" )
      cat.new( cmd, "\n" )
      system( cmd )
      cmd <- paste( blast.cmd, "-p blastn -d", file1, "-i", file2, addl.params )
    }
    else if ( length( seqs2 ) > length( seqs1 ) ) {
      cmd <- paste( formatdb.cmd, "-l", tmp.log.file, "-i", file2, "-p F -o T" )
      cat.new( cmd, "\n" )
      system( cmd )
      cmd <- paste( blast.cmd, "-p blastn -d", file2, "-i", file1, addl.params )
    }

    if ( ! full.out )
      cmd <- paste( cmd, "-m 8" )
    cat.new( cmd, "\n" )
    output <- system( cmd, intern=TRUE, ignore.stderr=TRUE )
  }
  ## Fields are: Query id, Subject id, % identity, alignment length, mismatches, gap openings,
  ## q. start, q. end, s. start, s. end, e-value, bit score"
  ##if ( ! full.out ) output <- strsplit( output, "\t" )
  return( output )
},

## Assumes blast.align was run with full.out=F                   
parse.blast.out = function( blast.out ) {
  out <- t( sapply( strsplit( blast.out, "\t" ), cbind ) )
  colnames( out ) <- c( "Query id", "Subject id", "% identity", "alignment length", "mismatches", "gap openings",
                       "q. start", "q. end", "s. start", "s. end", "e-value", "bit score" )
  return( out )
},

blast.match.seqs = function( seqs, match=NULL, e.cutoff=1 ) {
  if ( is.null( match ) ) match <- seqs
  out <- parse.blast.out( blast.align( seqs, match, paste( "-e", e.cutoff ) ) )
  good <- which( as.numeric( out[ ,"e-value" ] ) <= e.cutoff )
  out <- out[ good, c( "Query id", "Subject id" ) ]
  return( out )
}
)
try( detach( blast.util ) )
attach( blast.util )
