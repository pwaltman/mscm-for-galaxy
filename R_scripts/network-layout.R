try( detach( network.layout ), silent=T )
if ( exists( "network.layout" ) ) rm( network.layout )

network.layout <- list(

get.vizmap = function( nodes,
                       edges,
                       nsize=NULL,
                       nshape=NULL,
                       ncol=NULL,
                       nfill=NULL,
                       ntyp=NULL,
                       lcol=NULL,
                       lfont=NULL,
                       lsize=NULL,
                       loff.x=NULL,
                       loff.y=NULL,
                       ecol=NULL,
                       etyp=NULL,
                       ewid=NULL,
                       arrow=NULL,
                       asize=NULL,
                       opt=c( "default", "random" ) ) {

  node.count <- length( nodes )
  edge.count <- 0
  if ( ! is.null( edges ) ) edge.count <- nrow( edges )

  opt <- match.arg( opt )
  
  if ( opt == "random" ) {

    if ( is.null( nshape ) ) nshape <- sample( 21:25, node.count, rep=T )
    if ( is.null( nsize ) ) nsize <- runif( node.count ) * 2 + 0.5
    if ( is.null( ncol ) ) ncol <- sample( node.count )
    if ( is.null( nfill ) ) nfill <- sample( node.count )
    if ( is.null( ntyp ) ) ntyp <- sample( node.count )

    if ( is.null( lcol ) ) lcol <- sample( node.count )
    if ( is.null( lfont ) ) lfont <- sample( node.count )
    if ( is.null( lsize ) ) lsize <- runif( node.count ) * 2 + 0.5
    if ( is.null( loff.x ) ) loff.x <- 0
    if ( is.null( loff.y ) ) loff.y <- 0 #1/60 

    if ( is.null( ecol ) ) ecol <- sample( edge.count )
    if ( is.null( etyp ) ) etyp <- sample( edge.count )
    if ( is.null( ewid ) ) ewid <- ( runif( edge.count ) + 0.3 ) * 3
    if ( is.null( arrow ) ) arrow <- rep( 2, edge.count )
    if ( is.null( asize ) ) asize <- ( runif( edge.count ) + 0.2 ) * 0.1
    
  }
  else if ( opt == "default" ) {

    if ( is.null( nshape ) ) nshape <- 21
    ##if ( is.null( nshape ) ) nshape <- matrix(c(0,1,1,0,0,0,1,1),nrow=4,ncol=2)-0.5
    if ( is.null( nsize ) ) nsize <- 5
    if ( is.null( ncol ) ) ncol <- "black"
    if ( is.null( nfill ) ) nfill <- "darkgrey"
    if ( is.null( ntyp ) ) ntyp <- 1
    
    if ( is.null( lcol ) ) lcol <- "white"
    if ( is.null( lfont ) ) lfont <- 1
    if ( is.null( lsize ) ) lsize <- 1
    if ( is.null( loff.x ) ) loff.x <- 0
    if ( is.null( loff.y ) ) loff.y <- 0

    if ( is.null( ecol ) ) ecol <- rep( "darkgrey", edge.count )
    if ( is.null( etyp ) ) etyp <- rep( 1, edge.count )
    if ( is.null( ewid ) ) ewid <- rep( 1, edge.count )
    if ( is.null( arrow ) ) arrow <- rep( 0, edge.count )
    if ( is.null( asize ) ) asize <- rep( 0, edge.count )
  }

  out <- list( nodes=nodes,
               edges=edges,
               nsize=nsize,
               nshape=nshape,
               ncol=ncol,
               nfill=nfill,
               lcol=lcol,
               lfont=lfont, lsize=lsize,
               ntyp=ntyp,
               loff.x=loff.x, loff.y=loff.y, 
               ecol=ecol,
               etyp=etyp,
               ewid=ewid,
               arrow=arrow,
               asize=asize )
  return( out )
},

choose.node = function( node.names, node.coords ) {
  cat.new( "Select a node:\n" )
  node <- node.names[ identify( node.coords, n=1, plot=F ) ]
  return( node )
},

plot.network = function( node.names,
                         edges,
                         node.coords=NULL,
                         vizmap=NULL,
                         edge.sep.frac=150, ... ) {
  ow <- options( "warn" )
  options( warn=-1 )

  if ( is.null( vizmap ) )
    vizmap <- get.vizmap( node.names, edges )
  
  node.count <- length( node.names )

  if ( is.null( node.coords ) ) {
    node.coords <- matrix( 0, nrow=node.count, ncol=2 )
    node.coords[ ,1 ] <- ( runif( node.count ) - 0.5 ) * 100
    node.coords[ ,2 ] <- ( runif( node.count ) - 0.5 ) * 100
  }

  W <- diff( range( node.coords[ ,1 ] ) )
  L <- diff( range( node.coords[ ,2 ] ) )
  WL <- max( c( W, L ) )
  edge.sep = WL / edge.sep.frac

  plot( node.coords, type="n", ... ) ##, xlab="X", ylab="Y", ... )

  n.edges <- 0
  if ( ! is.null( edges ) ) n.edges <- nrow( edges )
  for ( i in 1:n.edges ) {
    if ( n.edges == 0 ) break
    node1 <- edges[ i, 1 ]
    node2 <- edges[ i, 2 ]
    if ( node1 == node2 ) next
    same.edges <- which( ( edges[ ,1 ] == node1 & edges[ ,2 ] == node2 ) |
                        ( edges[ ,1 ] == node2 & edges[ ,2 ] == node1 ) )

    ##if ( edges[ i, 1 ] == edges[ i, 2 ] ) next
    edge <- c( edges[ i, 1 ], edges[ i, 2 ] )
    if ( length( same.edges ) > 1 ) {
      ll <- length( same.edges )
      for ( e in 1:ll ) {
        if ( all( edges[ same.edges[ e ], ] == edge ) ) {
          ind <- same.edges[ e ]
          off <- norm.unit.vector( node.coords[ edge, 1 ], node.coords[ edge, 2 ] )
          xs <- node.coords[ edge, 1 ] + (edge.sep * off[1] * ( as.integer( ll/2 ) - e ))
          ys <- node.coords[ edge, 2 ] + (edge.sep * off[2] * ( as.integer( ll/2 ) - e ))
          if ( is.null( vizmap$arrow ) || vizmap$arrow[ind] <= 0 )
            lines( xs, ys, col=vizmap$ecol[ind], lty=vizmap$etyp[ind], lwd=vizmap$ewid[ind] )
          else arrows( xs[1], ys[1], xs[2], ys[2],
                      code=vizmap$arrow[ind], length=vizmap$asize[ind],
                      col=vizmap$ecol[ind], lty=vizmap$etyp[ind], lwd=vizmap$ewid[ind] )
          #break
        }
      }
    } else {
      xs <- node.coords[ edge, 1 ]
      ys <- node.coords[ edge, 2 ]
      if ( is.null( vizmap$arrow ) || vizmap$arrow[i] <= 0 )
        lines( xs, ys, col=vizmap$ecol[i], lty=vizmap$etyp[i], lwd=vizmap$ewid[i] )
      else arrows( xs[1], ys[1], xs[2], ys[2],
                 code=vizmap$arrow[i], length=vizmap$asize[i],
                 col=vizmap$ecol[i], lty=vizmap$etyp[i], lwd=vizmap$ewid[i] )
    }
  }

  if ( ! is.null( vizmap$nshape ) && is.matrix( vizmap$nshape ) ) { 
    ## If nshape is a 2-column matrix, then plot them as a polygon
    ## DOESNT WORK YET!!!
    for ( i in 1:nrow( node.coords ) ) {
      ii <- i
      if ( length( vizmap$nshape ) == 1 ) ii <- 1
      polygon( vizmap$nshape[ ii ] +
              vizmap$nsize * matrix( node.coords[ 1, ], ncol=2,
                                    nrow=nrow( vizmap$nshape[ ii ] ), byrow=T ),
              col=vizmap$nfill, border=vizmap$ncol, lty=vizmap$ntyp )
    }
  } else {
    ## Use the standard point types
    points( node.coords, pch=vizmap$nshape, cex=vizmap$nsize, col=vizmap$ncol,
           bg=vizmap$nfill, ... )
  }
  text.coords <- node.coords
  text.coords[ ,1 ] <- text.coords[ ,1 ] + WL * vizmap$loff.x
  text.coords[ ,2 ] <- text.coords[ ,2 ] + WL * vizmap$loff.y
  text( text.coords, labels=node.names, xpd=NA, col=vizmap$lcol,
       font=vizmap$lfont, cex=vizmap$lsize )

  options( ow )
  invisible( node.coords )
},

CircleGraphLayout = function( nodes, radius=100 ) {
	
  node.count <- length( nodes )

  r <- radius
  phi <- 2 * pi / node.count

  node.coords <- matrix( 0, nrow=node.count, ncol=2 )

  ## Arrange vertices in a circle
  node.coords[ , 1 ] <- r * sin( 1:node.count * phi )
  node.coords[ , 2 ] <- r * cos( 1:node.count * phi )
  
  invisible( node.coords )
},

norm.unit.vector = function( xs, ys ) {
  slope <- ( ys[ 2 ] - ys[ 1 ] ) / ( xs[ 2 ] - xs[ 1 ] )
  norm.slope <- - 1 / slope
  out <- c( 1, norm.slope )
  s <- sqrt( sum( out**2 ) )
  out <- out / s
  return( out )
}
)

attach( network.layout )
