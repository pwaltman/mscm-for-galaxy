library( GO.db )
source( 'R_scripts/validation/valUtils2.R' )

gen.enriched.go.term.tbl <- function ( goid.recs,
                                       organism=get.global( 'organism' ),
                                       clusterstack=get.global( 'clusterStack' ),
                                       params=get.global( 'params' ),
                                       gene.ontology.fname="data/validation/gene_ontology_edit.obo",
                                       gene.ref="http://www.ncbi.nlm.nih.gov/sites/entrez?db=gene&amp;cmd=search&amp;term=",
                                       goid.ref="http://amigo.geneontology.org/cgi-bin/amigo/go.cgi?view=details&search_constraint=terms&depth=0&query=",
                                       vP=get.global( 'vP' )
                                      ) {

  loci.to.prot.name.map <- get.prot.names.db( gene.ids=1:get.num.genes.db( organism ),
                                              organism=organism )
  names( loci.to.prot.name.map ) <- get.gene.names.db( gene.ids=1:get.num.genes.db( organism ),
                                                       organism )
  whch <- which( loci.to.prot.name.map == "-" )
  loci.to.prot.name.map[ whch ] <- names( loci.to.prot.name.map )[ whch ]
  
  htmlTableRows <- sapply( names( goid.recs ),
                           function(i) {
                             sapply( goid.recs[[ i ]],
                                     function(j) {
                                       
                                       if ( is.na( j$pval ) || j$pval > 0.01 )
                                         return( NULL )

                                       sorted.inds <- sort( loci.to.prot.name.map[ j$genes ], ind=T )$ix
                                       paste( as.numeric(i),
                                              paste( paste( "<a href=\"",
                                                            goid.ref,
                                                            j$goid,
                                                            "\" target=\"_blank\"\\>",
                                                            sep=""
                                                           ),
                                                     j$goid, "</a>", sep="" ),
                                              j$term,
                                              j$pval,
                                              paste( paste( paste( "<a href=\"", gene.ref, sep="" ),
                                                            paste(j$genes[ sorted.inds ], "\" target=\"_blank\">", sep="" ),
                                                            loci.to.prot.name.map[ j$genes[ sorted.inds ] ],
                                                            "</a>",
                                                            sep=""),
                                                     collapse=", " ),
                                              sep="</td><td>"
                                             )
                                     }
                                    )
                           }
                          )
  htmlTableRows <- unlist( htmlTableRows ); names( htmlTableRows ) <- NULL

  tmp <- c( "<table class=\"sortable\", id=\"sorttable\">",
            "<tr><th>Cluster</th><th>GOID</th><th>GO Term</th><th>p-value</th><th>Genes</th></tr>")

  for (i in 1:length(htmlTableRows)){
    if (i %% 2)
      tmp <- c(tmp, paste("<tr class=\"d0\"><td>", htmlTableRows[i], "</td></tr>", sep=""))
    else
      tmp <- c(tmp, paste("<tr class=\"d1\"><td>", htmlTableRows[i], "</td></tr>", sep=""))
  }
  tmp <- c(tmp, "</table>")

  x <- readLines( vP$table.html )
  x <- gsub( "INFO", "GO enrichment", x)
  x <- gsub( "SPECIES", vP$species, x)
  

  if ( exists( 'organisms', '.GlobalEnv' ) )
    meta <- paste( get.global( 'organisms' ), sep=":", collapse="-" )
  else
    meta <- organism
  
  x <- gsub( "META", meta, x)
	
  x <- gsub( "TABLE", paste(tmp, sep="", collapse="\n"), x )

  if ( ! file.exists( vP$val.out.dir ) )
    dir.create( vP$val.out.dir, recursive=T )
  
  cat( x,
       file=paste( vP$val.out.dir, paste(vP$organism, "go.enrch.table.html", sep="."),  sep="/"),
       sep="\n",
       collapse="\n" )
  cat.new( "made go enrichment table.html\n", log=vP$val.log, append=TRUE )
  cat.new( "GO enrichment ended\n", date(), "\n", log=vP$val.log, append=TRUE )
}



get.gotermfinder.goids <- function( organism=get.global( 'organism' ),
                                    clusterStack=get.global( 'clusterStack' ),
                                    params=get.global( 'params' ),
                                    gene.ontology.fname="data/validation/gene_ontology_edit.obo",
                                    mode='elaborated'
                                   ) {
  run.analyze.pl( organism=organism,
                  clusterStack=clusterStack,
                  params=params,
                  gene.ontology.fname=gene.ontology.fname,
                  mode=mode
                 )
  return( parse.analyze.pl.result( organism=organism,
                                   gene.ontology.fname )
         )
}

parse.term.record <- function( goid.line,
                               comp.lines,
                               t.lines ){

  if ( length( grep( "^GOID", t.lines[ goid.line ] ) ) == 0 )
    stop( "wrong starting line number given to parse.term.record\n" )

  
  goid <- strsplit( t.lines[ goid.line ], "\t" )[[1]]
  goid <- goid[ grep( "^GO:", goid ) ]

  if ( goid.line > comp.lines[1] && goid.line < comp.lines[2] )
    comp <- "BP:"
  if ( goid.line > comp.lines[2] && goid.line < comp.lines[3] )
    comp <- "CC:"
  if ( goid.line > comp.lines[3] )
    comp <- "MF:"

  
  term <- paste( comp, strsplit( t.lines[ goid.line+1 ], "\t" )[[1]][2] )

  cor.pval <- as.numeric( strsplit( t.lines[ goid.line+2 ], "\t" )[[1]][2] )

  anno.genes <- strsplit( t.lines[ goid.line+8 ], ", " )[[1]]

  retVal <- list( goid=goid,
                  term=term,
                  pval=cor.pval,
                  genes=anno.genes )
}

parse.analyze.pl.result <- function( organism=get.global( 'organism' ),
                                     gene.ontology.fname="data/validation/gene_ontology_edit.obo" ) {

  terms.files <- list.files( output.dir, pattern=".terms", full.names=T )
  if ( length( terms.files ) == 0 ) {
    cat( "no terms files found in", output.dir, "for", organism, "\n" )
    return( NULL )
  }

  retVal <- list()
  for ( i in 1:length( terms.files ) ) {
    t.lines <- readLines( terms.files[i] )
    goid.lines <- grep( "^GOID", t.lines )
    comp.lines<- grep( "^Finding terms for", t.lines )
    go.recs <- lapply( goid.lines, parse.term.record, comp.lines, t.lines )
    names( go.recs ) <- sapply( go.recs, "[[", "goid" )
    retVal[[ i ]] <- go.recs
  }
  names( retVal ) <- strsplit( sapply( strsplit( terms.files, "cluster" ),
                                       function(i) i[[2]] ),
                               ".txt.terms" )

  go.terms <- sort( unique( unlist( sapply( retVal, function(i) names( i ) ) ) ) )
  goid.mat <- matrix( 1,
                      nr=length( terms.files ),
                      nc=length( go.terms )
                     )

  colnames( goid.mat ) <- go.terms
  rownames( goid.mat ) <- names( retVal )

  for ( i in 1:length( retVal ) ) {
    if ( length( retVal[[ i ]] ) == 0 )
      next
    goid.mat[ i, names( retVal[[ i ]] ) ] <- sapply( retVal[[i]], "[[", "pval" )
  }

  for ( i in 1:length( terms.files )  ) {
    unlink( terms.files[i] )
    terms.files[ i ] <- gsub( ".terms", "", terms.files[i] )
    unlink( terms.files[i] )
  }
  
  return( list( goid.recs=retVal,
                goid.mat=goid.mat )
         )
}


run.analyze.pl <- function( organism=get.global( 'organism' ),
                            clusterStack=get.global( 'clusterStack' ),
                            params=get.global( 'params' ),
                            gene.ontology.fname="data/validation/gene_ontology_edit.obo",
                            mode='elaborated'
                           ) {

  if ( organism %in% names( clusterStack ) )
    clusterStack <- clusterStack[[ organism ]]

  new.goa.file <- gen.goa.file( org=organism, params=params, mode=mode )
  ##stop( 'phw dbg\n' )
  if ( mode == 'shared' ) {
    if ( "olog.pairs" %in% ls( ".GlobalEnv" ) ) {
      olog.pairs <- get.global( 'olog.pairs' )
      nGenes <- length( unique( olog.pairs[[ organism ]] ) )
    }
    else
      stop( "olog.pairs not available for run.analyze.pl in shared mode\n" )
  }
  else
    nGenes <- get.num.genes.db( organism )

  cl.files <- character()
  for ( i in 1:length( clusterStack ) ) {
    fname <- paste( output.dir,
                    paste( organism,
                           "cluster",
                           sprintf( "%04d", as.integer(i) ),
                           ".txt",
                           sep=""
                          ),
                    sep="/"
                   )

    if ( file.exists( fname ) )
      unlink( fname )
    
    if ( clusterStack[[ i ]]$nrows > 3 ) {
      cat( get.gene.names.db( clusterStack[[i]]$rows, organism ),
           "\n",
           sep="\n",
           file=fname
          )
      cl.files <- c( cl.files, fname )
    }
  }

  cmd <- paste( paste( "analyze.pl", new.goa.file, nGenes, gene.ontology.fname ),
                paste( cl.files, collapse=" " )
               )
  
  system( cmd )
  unlink( new.goa.file )
  return( cmd )
}



run.batchGoView <- function( organism=get.global( 'organism' ),
                             clusterStack=get.global( 'clusterStack' ),
                             params=get.global( 'params' ),
                             vP=get.global( "vP" ),
                             mode="elaborated",                            
                             gene.ontology.fname="~/cMonkey/shared/data/validation/gene_ontology_edit.obo",
                             aspect='P'
                            ) {
  if ( organism %in% names( clusterStack ) )
    clusterStack <- clusterStack[[ organism ]]

  goview.res <- make.goterm.conf( organism=organism,
                                  params=params,
                                  vP=vP,
                                  aspect=aspect
                                 )
  conf.fname <- goview.res$conf.fname
  goview.dir <- goview.res$goview.dir
  file.symlink( gene.ontology.fname, goview.dir )

  cl.files <- character()
  for ( i in 1:length( clusterStack ) ) {
    fname <- paste( output.dir,
                    paste( organism,
                           "cluster",
                           sprintf( "%04d", as.integer(i) ),
                           ".txt",
                           sep=""
                          ),
                    sep="/"
                   )

    if ( file.exists( fname ) )
      unlink( fname )
    
    cat( get.gene.names.db( clusterStack[[i]]$rows, organism ),
         "\n",
         sep="\n",
         file=fname
        )
    cl.files <- c( cl.files, fname )
  }
  cmd <- paste( paste( "batchGOView.pl", conf.fname ),
                paste( cl.files, collapse=" " )
               )
  
  system( paste( paste( "batchGOView.pl", conf.fname ), 
                 paste( cl.files, collapse=" " )
                )
         )

  ##  Now do a little clean-up
  file.rename( paste( goview.dir, "batchGOView.html", sep="/" ),
               paste( goview.dir, "index.html", sep="/" )
              )
  for ( cl.file in cl.files )
    unlink( cl.file )
  ## try to find and remove the symbolic link we
  ## created to the gene.ontology.fname
  lscmd <- paste( "ls -F", goview.dir )
  tmp <- system( lscmd, intern=T )
  if ( length( grep( "@", tmp ) ) > 0 )
    tmp <- tmp[ grep( "@", tmp ) ]
    tmp <- substring( tmp, 1, nchar( tmp )-1 )
    file.remove( paste( goview.dir,
                        tmp,
                        sep="/" )
                )
  
  return( cmd )
}


make.goterm.conf <- function( fname=NULL,
                              organism=get.global( 'organism' ),
                              params=get.global( 'params' ),
                              vP=get.global( "vP" ),
                              mode="elaborated",
                              aspect='P'
                             ) {
  if ( is.null( fname ) ) {
    
    goview.dir <- paste( vP$val.out.dir,
                         "goview",
                         switch( aspect, "P"="process", "F"="function", "C"="component" ),
                         sep="/"
                        )
    
    fname <- paste( goview.dir,
                    paste( aspect, "GoView.conf", sep="." ),
                    sep="/"
                   )
    if ( ! file.exists( goview.dir ) ) {
      dir.create( goview.dir, recursive=T )
    }
  }

  if ( file.exists( fname ) )
    unlink( fname )

  
  cat( "maxNode = 30", "\n", file=fname, append=T )
  cat( "minMapWidth = 350", "\n", file=fname, append=T )
  cat( "minMapHeight4TopKey = 600", "\n", file=fname, append=T )
  cat( "minMapWidth4OneLineKey = 620", "\n", file=fname, append=T )
  cat( "widthDisplayRatio = 0.8", "\n", file=fname, append=T )
  cat( "heightDisplayRatio = 0.8", "\n", file=fname, append=T )
  cat( "binDir = ", "\n", file=fname, append=T )
  cat( "libDir = ", "\n", file=fname, append=T )
  cat( "mapNote = ", "\n", file=fname, append=T )
  cat( "pvalueCutOff = 0.01", "\n", file=fname, append=T )
  cat( "calculateFDR = 1", "\n", file=fname, append=T )
  cat( "makePs = 0", "\n", file=fname, append=T )
  cat( "ontologyFile = gene_ontology_edit.obo",
       "\n", file=fname, append=T )
  
  cat( paste( "annotationFile =",
              gen.goa.file( goview.dir, organism, params, mode=mode )
             ),
       "\n", file=fname, append=T )

  cat( "aspect =", aspect, "\n", file=fname, append=T )

  if ( mode == "shared" ) {
    if ( "olog.pairs" %in% ls( ".GlobalEnv" ) ) {
      olog.pairs <- get.global( 'olog.pairs' )
      cat( "totalNumGenes =", length( unique( olog.pairs[[ organism ]] ) ), "\n", file=fname, append=T )
    }
    else
      stop( "olog.pairs not in .GlobalEnv for make.goterm.conf while processing shared clusterStack\n" )
  }
  else
    cat( "totalNumGenes =", get.num.genes.db( organism ), "\n", file=fname, append=T )


  cat( "outDir = \n",
       file=fname, append=T )
  cat( "geneUrl = http://www.ncbi.nlm.nih.gov/sites/entrez?db=gene&cmd=search&term=<REPLACE_THIS>", "\n",
       file=fname, append=T )      
  cat( "goidUrl = http://amigo.geneontology.org/cgi-bin/amigo/go.cgi?view=details&search_constraint=terms&depth=0&query=<REPLACE_THIS>",
       "\n",
       file=fname, append=T )

  return( list( goview.dir=goview.dir,
                conf.fname=fname )
         )
}

gen.goa.file <- function( goview.dir=NULL,
                          organism=get.global( "organism" ),
                          params=get.global( "params" ),
                          mode='elaborated'
                         ) {

  if ( is.null( goview.dir ) )
    new.goa.fname <- paste( organism, "goview.updated.goa", sep="." )
  else
    new.goa.fname <- paste( goview.dir,
                            paste( organism, "goview.updated.goa", sep="." ),
                            sep="/"
                           )
  
  if ( file.exists( new.goa.fname ) )
    return( paste( organism, "goview.updated.goa", sep="." ) )

  if ( ! file.exists( output.dir ) )
    dir.create( output.dir )

  ## we've added workaround code to address possible timeout issue from ftp.ebi.ac.uk
  ## UPDATE on 06.02.2011 by phw: bsu, banth, lmo, eco, stm, vch files have since been updated
  if ( organism %in% c( 'banth', 'lmo', 'ecoli', 'stm', 'vch' ) ) { ##, 'bsubt' ) {
    
    if ( organism == 'lmo' ) {
      ##goa <- read.delim( "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/68.L_monocytogenes_1-2a.goa", head=F, skip=1 )
      ##goa <- read.delim( "data/68.L_monocytogenes_EGD-e.goa", head=F, skip=1 )
      tryres <- try( goa <- read.delim( "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/68.L_monocytogenes_EGD-e.goa", head=F, skip=1 ),
                     silent=T )
      if ( class( tryres ) == "try-error" ) {
        goa <- read.delim( gzfile( params$read[[ organism ]]$goa.file ), head=F, skip=1 )
      }
      goa$V11 <- toupper( as.character( goa$V11 ) )
    }

    if ( organism == "banth" ) {
      ## try to download the latest version - if there's an error, use the one we have on hand
      tryres <- try( goa <- read.delim( "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/136.B_anthracis_Ames.goa", head=F, skip=1 ),
                     silent=T )
      if ( class( tryres ) == "try-error" ) {
        goa <- read.delim( gzfile( params$read[[ organism ]]$goa.file ), head=F, skip=1 )
      }
      goa$V11 <- sapply( strsplit( as.character( goa$V11 ), "_" ), function(x) paste( x, collapse="" ) )
    }

    ##  don't use the latest file for bsubt - genome annotations have changed too much
    ##   i.e. too many new genes (genes that don't end w/a '0' and too many genes have been given new BSU loci
    if ( organism == "bsubt" ) {
      ## try to download the latest version - if there's an error, use the one we have on hand
      tryres <- try( goa <- read.delim( "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/6.B_subtilis.goa", head=F, skip=1  ),
                     silent=T )
      if ( class( tryres ) == "try-error" ) {
        goa <- read.delim( gzfile( params$read[[ organism ]]$goa.file ), head=F, skip=1 )
      }
      ## this line was for publication purposes, but with new data set from ARB, we can use the latest files
      ##goa <- read.delim( "data/6.B_subtilis.goa", head=F, skip=1 ) 
    }
    if ( organism == "ecoli" ) {
      ## try to download the latest version - if there's an error, use the one we have on hand
      tryres <- try( goa <- read.delim( "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/18.E_coli_MG1655.goa", head=F, skip=1 ),
                     silent=T )
      if ( class( tryres ) == "try-error" ) {
        goa <- read.delim( gzfile( params$read[[ organism ]]$goa.file ), head=F, skip=1 )
      }
      goa$V11 <- toupper( as.character( goa$V11 ) )
    }
    if ( organism == "stm" ) {
      ## try to download the latest version - if there's an error, use the one we have on hand
      tryres <- try( goa <- read.delim( "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/69.S_typhimurium_ATCC_700720.goa", head=F, skip=1 ),
                     silent=T )
      if ( class( tryres ) == "try-error" ) {
        goa <- read.delim( gzfile( params$read[[ organism ]]$goa.file ), head=F, skip=1 )
      }
      goa$V11 <- toupper( as.character( goa$V11 ) )
    }
    if ( organism == "vch" ) {
      ## try to download the latest version - if there's an error, use the one we have on hand
      tryres <- try( goa <- read.delim( "ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/46.V_cholerae_ATCC_39315.goa", head=F, skip=1 ),
                     silent=T )
      if ( class( tryres ) == "try-error" ) {
        goa <- read.delim( gzfile( params$read[[ organism ]]$goa.file ), head=F, skip=1 )
      }
      goa$V11 <- toupper( as.character( goa$V11 ) )
    }
    
  }
  else {
    goa <- read.delim( gzfile( params$read[[ organism ]]$goa.file ), head=F, skip=1 )
  }

  if ( length( grep( "^mouse|human", organism ) ) > 0 ) {

    goa <- as.matrix( goa )
    goa[,3] <- toupper( goa[, 3] )
    
    mycon <- db.open.con( organism )
    org.genes <- as.character( dbReadTable( mycon, "genes" )$gene_name )
    dbDisconnect( mycon )
    
    ok.goa <- goa[ ( toupper( goa[,3] ) %in% org.genes ), ]
    to.fix.goa <- goa[ ( ! goa[,3] %in% org.genes ), ]
    fd.genes <- strsplit( to.fix.goa[,11], "\\|" )
    org.genes <- org.genes[ ! org.genes %in% ok.goa[,3] ]
    fixed <- sapply( fd.genes, function(x) ifelse( any( x %in% org.genes ), x[ x %in% org.genes ], NA ) )
    to.fix.goa <- as.matrix( to.fix.goa )
    to.fix.goa <- to.fix.goa[ ! is.na( fixed ), ]
    fixed <- fixed[ ! is.na( fixed ) ]
    to.fix.goa[,3] <- fixed

    goa <- rbind( as.matrix( ok.goa ), to.fix.goa )
    goa[,2] <- goa[,11] <- goa[,3]
  }
  else {
    fd.genes <- strsplit( as.character( goa[,11] ), "\\|" )
    fd.genes <- lapply( fd.genes, function(x) unique( x[ grep( params$read[[ organism ]]$valid.gene.regexp, x ) ] ) )
    n.hits <- sapply( fd.genes, length )
    counts <- as.integer( names( table( as.factor( n.hits ) ) ) )
    
    new.genes <- character()
    new.order <- integer()
    for ( i in counts[ counts > 0 ] ) {
      new.order <- c( new.order,
                      sort( rep( which( n.hits %in% i ), i ) ) )
      new.genes <- c( new.genes,
                      unlist( fd.genes[ which( n.hits %in% i ) ] ) )
    }

    goa <- goa[ new.order,]
    goa[,11] <- new.genes

    goa[,2] <- goa[,3] <- goa[,11]
  }
  
  
  if ( mode == 'shared' ) {
    olog.pairs <- get.global( 'olog.pairs' )
    org.olog <- get.gene.names.db( unique( olog.pairs[[ organism ]] ), organism )
    goa <- goa[ (goa[,11] %in% org.olog ), ]
  }
  
  write.table( goa,
               new.goa.fname,
               quote=F,
               sep="\t",
               row.names=F,
               col.names=F
              )
  return( paste( organism, "goview.updated.goa", sep="." ) )
}


compare.shared.and.elabd.goid <- function( shared.goid=get.global( 'shared.goid' ),
                                           elabd.goid=get.global( 'elabd.goid' ),
                                           thresh=0.1
                                          ) {

  org.pairs <- names( shared.goid )
  if ( ! all( org.pairs %in% names( elabd.goid ) ) )
    stop( "incomparable shared/elabd goid structs given to compare.shared.and.elabd.goid\n" )

  shared.org.to.org <- list()
  org.shared.to.elabd <- list()
  for ( pair in org.pairs ) {

    shared.org.to.org[[ pair ]] <- list()
    organisms <- names( shared.goid[[ pair ]] )

    shared.org.to.org[[ pair ]][[ 'shared.goid' ]] <- list()
    shared.org.to.org[[ pair ]][[ 'unique.goid' ]] <- list()
    for ( org in organisms )
      shared.org.to.org[[ pair ]][[ 'unique.goid' ]][[ org ]] <- list()
    
    for ( i in 1:nrow( shared.goid[[ pair ]][[ organisms[1] ]]$goid.mat ) ) {

      o1.gs <- names( which( shared.goid[[ pair ]][[ organisms[1] ]]$goid.mat[ i, ] < thresh ) )
      o2.gs <- names( which( shared.goid[[ pair ]][[ organisms[2] ]]$goid.mat[ i, ] < thresh ) )

      if ( length( o1.gs[ o1.gs %in% o2.gs ] ) > 0 )
        shared.org.to.org[[ pair ]][[ 'shared.goid' ]][[ as.character(i) ]] <- o1.gs[ o1.gs %in% o2.gs ]
#      else
#        shared.org.to.org[[ pair ]][[ 'shared.goid' ]][[ i ]] <- NA

      if ( length( o1.gs[ !o1.gs %in% o2.gs ] ) > 0 )
        shared.org.to.org[[ pair ]][[ 'unique.goid' ]][[ organisms[1] ]][[ as.character( i ) ]] <- o1.gs[ !o1.gs %in% o2.gs ]
#      else
#        shared.org.to.org[[ pair ]][[ 'unique.goid' ]][[ organisms[1] ]][[ i ]] <- NA

      if ( length( o2.gs[ !o2.gs %in% o1.gs ] ) > 0 )
        shared.org.to.org[[ pair ]][[ 'unique.goid' ]][[ organisms[2] ]][[ as.character( i ) ]] <- o2.gs[ !o2.gs %in% o1.gs ]
#      else
#        shared.org.to.org[[ pair ]][[ 'unique.goid' ]][[ organisms[2] ]][[ i ]] <- NA
    }


    org.shared.to.elabd[[ pair ]] <- list()
    for ( org in organisms ) {
      org.shared.to.elabd[[ pair ]][[ org ]] <- list()
      for ( i in 1:nrow( shared.goid[[ pair ]][[ org ]]$goid.mat ) ) {
        org.shared.to.elabd[[ pair ]][[ org ]][[ i ]] <- list()
        s.gs <- names( which( shared.goid[[ pair ]][[ org ]]$goid.mat[ i, ] < thresh ) )
        e.gs <- names( which( elabd.goid[[ pair ]][[ org ]]$goid.mat[ i, ] < thresh ) )

        if ( length( s.gs[ s.gs %in% e.gs ] ) > 0 )
          org.shared.to.elabd[[ pair ]][[ org ]][[ i ]][[ 'kept' ]] <- s.gs[ s.gs %in% e.gs ]
        else
          org.shared.to.elabd[[ pair ]][[ org ]][[ i ]][[ 'kept' ]] <- NA

        if ( length( s.gs[ ! s.gs %in% e.gs ] ) > 0 )
          org.shared.to.elabd[[ pair ]][[ org ]][[ i ]][[ 'dropt' ]] <- s.gs[ ! s.gs %in% e.gs ]
        else
          org.shared.to.elabd[[ pair ]][[ org ]][[ i ]][[ 'dropt' ]] <- NA

        if ( length( e.gs[ ! e.gs %in% s.gs ] ) > 0 )        
          org.shared.to.elabd[[ pair ]][[ org ]][[ i ]][[ 'new' ]] <- e.gs[ ! e.gs %in% s.gs ]
        else
          org.shared.to.elabd[[ pair ]][[ org ]][[ i ]][[ 'new' ]] <- NA
      }
    }
      
  }
  return( list( shared.org.to.org=shared.org.to.org,
                org.shared.to.elabd=org.shared.to.elabd )
         )
}


test.fn <- function( comps,
                     thresh=0.05,
                     shared.goid=get.global( "shared.goid" )
                    ) {

  shared.org.to.org <- comps$shared.org.to.org 
  retVal <- list()
  for ( pair in names( shared.goid ) ) {

    retVal[[ pair ]] <- list()
    orgs <- names( shared.goid[[ pair ]] )

    bicls.with.ovp <- names( shared.org.to.org[[ pair ]]$shared.goid )

    for ( org in orgs ) {    

      retVal[[ pair ]][[ org ]] <- list()

      bicls.with.unique <- names( shared.org.to.org[[ pair ]]$unique.goid[[ org ]] )

      no.ovp <- bicls.with.unique[ ! bicls.with.unique %in% bicls.with.ovp ]
      if ( length( no.ovp ) > 0 )
        retVal[[ pair ]][[ org ]][ no.ovp ] <- 0


      all.ovp <- bicls.with.ovp[ ! bicls.with.ovp %in% bicls.with.unique  ]
      if ( length( all.ovp ) > 0 )
        retVal[[ pair ]][[ org ]][ all.ovp ] <- 1


      partial.ovp <- bicls.with.ovp[ bicls.with.ovp %in% bicls.with.unique ]
      for( partial.id in partial.ovp ) {

        num.sh <- length( shared.org.to.org[[ pair ]]$shared.goid[[ partial.id ]] )
        num.not <- length( shared.org.to.org[[ pair ]]$unique.goid[[ org ]][[ partial.id ]] )

        retVal[[ pair ]][[ org ]][[ partial.id ]] <- ( num.sh/( num.sh + num.not ) )
      }
      ## now just sort the list numerically by the cluster id
      retVal[[ pair ]][[ org ]] <- retVal[[ pair ]][[ org ]][ sort( as.numeric( names( retVal[[ pair ]][[ org ]] ) ), index=T )$ix ]
    }
  }

  return( retVal )
}

depth.of.GO.term <- function( term ) {

  if ( term == "GO:XXXXXXX" )
    return( 0 )
  
  aspects <- c( "C", "F", "P" )

  for (aspect in aspects ) {
    cmd <- paste( "ancestors.pl", term, "data/validation/gene_ontology_edit.obo", aspect )
    tmp <- system( cmd, intern=T )

    if ( length( grep( "^GO", tmp ) > 0 ) ){
      head.nodes <- grep( "^GO", tmp )

      if ( length( head.nodes ) > 1 ) {
        max.diff <- 0
        for ( i in 2:length( head.nodes ) )
          max.diff <- max( head.nodes[i] - head.nodes[ i-1 ], max.diff )

      }
      else
        max.diff <- length( tmp )
      
      ## subtract 1 for the blank
      return( max.diff-1 )
    }
    else
      next
  }
}


get.sig.GO.depths <- function( all.go.mat,
                               depths=NULL,
                               pval=0.05 ) {
  bak <- depths
  
  if ( is.null( depths ) )
    depths <- sapply( colnames( all.go.mat ), depth.of.GO.term )
  else {
    if ( sum( names( depths ) %in% colnames( all.go.mat  ) ) != ncol( all.go.mat ) ) {

      ovp <- names( depths )[ names( depths ) %in% colnames( all.go.mat ) ]
      unknown <- colnames( all.go.mat )[ ! colnames( all.go.mat ) %in% ovp ]
      cat( "need to find", length( unknown ), "depths\n" )
      
      partial <- sapply( unknown, depth.of.GO.term )

      depths <- depths[ ovp ]
      depths <- c( depths, partial )
      depths <- depths[ sort( names( depths ), index=T )$ix ]
    }
  }

  bicl.go.depts <- matrix( 0,
                           nr=nrow( all.go.mat ),
                           nc=ncol( all.go.mat )
                          )

  for( i in 1:nrow( bicl.go.depts ) ) {

    whch <- which( all.go.mat[ i, ] <= pval )

    if ( length( whch ) > 0 )
      bicl.go.depts[ i, whch ] <- depths[ names( whch ) ]
  }

  max.depths <- apply( bicl.go.depts, 1, max )
  bak <- bak[ ! names( bak ) %in% names( depths ) ]
  depths <- c( bak, depths )
  depths <- depths[ sort( names( depths ), index=T )$ix ]
  
  return( list( max.depths=max.depths,
                depths=depths )
         )
}
