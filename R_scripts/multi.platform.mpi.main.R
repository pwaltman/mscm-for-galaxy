## 2011-05 PHW

organisms <- character()
mode <- "basis"

shell.params <- as.matrix( read.csv( "rparams.txt", head=F ) )
organisms <- shell.params[,2][ shell.params[,1] %in% "org" ]
if ( "mode" %in% shell.params[,1] )
  mode <- shell.params[,2][ shell.params[,1] %in% "mode" ]
if ( "basis" %in% shell.params[,1] )
  basis <- shell.params[,2][ shell.params[,1] %in% "basis" ]

if ( length( organisms ) > 2 )
  organisms <- organisms[ 1:2 ]


if ( system( "uname -n", intern=T ) == "billy" ) {
  load( '/home/waltman/cMonkey/multi.species/branches/pseudo.multi.platform/pseudo.multi.platform.seed.pairings.rda' )
} else {
  load( '/home/pw532/cMonkey/multi.species/branches/old.pseudo.multi.platform/pseudo.multi.platform.seed.pairings.rda' )
}
basis.pairs <- get( basis )

if ( length( organisms ) == 0 ) {
  stop( "no organisms specifed\n", file=fname )
}

cat( "organisms in basis.pair.mpi.main.R", organisms, "\n" )

options( warn=-1 )

if ( length( organisms ) == 2 ) {
  if ( mode == "basis" ) {
    source( "R_scripts/basis.pair.main.R" )
  }
  else {
    if ( length( grep( "aug", mode ) ) > 0 ) {
      source( "R_scripts/augment.main.R" )
    }
    else {
      if ( length( grep( "extend", mode ) ) > 0 ) {
        source( "R_scripts/extend.elab.main.R" )
      }
      else
        stop( "invalid mode specified for multiple organisms", mode, "\n" )
    }
  }
}

if ( length( organisms ) == 1 ) {
  organism <- organisms
  rm( organisms )
  source( "R_scripts/single.main.R" )
}

