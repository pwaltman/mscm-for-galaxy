## Get the default params and then override some of them here...

humanParams <- function() {

  human <- list()

  human$organism <- "human"
  human$species <- "Homo Sapien"
  human$data.types.used  <- c( "expression", "network" ) # , "network", "seq.motif" )
  human$is.eukaryotic <- T
  
####################### SINGLE-SPECIES OPT'N PARAMS ############################

  ##human$single <- list()
  ##  phw comment: a little redundant, sigh, but I don't know how to make the $single list inherit
  ##               these from the main human list
  human$single$organism <- human$organism
  human$single$species <- human$species 
  human$single$data.types.used  <- human$data.types.used
  human$single$is.eukaryotic <- human$is.eukaryotic
  
  human$single$kmax <- 200
  human$single$row.ceiling <- 175 ##50 ## 75  human$ ## 125, 100 ?
  human$single$max.clust.per.gene <- 3
  human$single$row.thresh <- 0.5
  human$single$output.dir <- paste( "output/", human$single$organism, "/", sep="" )
  human$single$log.dir <- paste( human$single$output.dir, "logs/", sep="" )
  
####################### DATA READ-IN FILES ###############################

  human$read <- list()

  ##  phw comment: a little redundant, sigh, but I don't know how to make the $read list inherit
  ##               these from the main human list
  human$read$organism <- human$organism
  human$read$species <- human$species 
  human$read$data.types.used  <- human$data.types.used
  human$read$is.eukaryotic <- human$is.eukaryotic

  
  human$read$data.dir <- paste( "data/", human$organism, "/", sep="" )
  human$read$log.dir <- paste( human$read$output.dir, "logs/", sep="" )
  human$read$ratios.fname <- paste( human$read$data.dir, "ratios.table.txt.gz", sep="" )
  human$read$seqs.fname <- paste( human$read$data.dir, "human.upstream.3kb.fna.gz", sep="" )
  human$read$met.net.file  <- paste( human$read$data.dir, "/kegg_gene_name_siff.txt", sep="" )
  human$read$pp.interaction.sif <- paste( human$read$data.dir, "/biogrid_050408.txt.gz", sep="" )
  human$read$tf.list.file <- NULL 
  human$read$prolinks.file <- paste( human$read$data.dir, "/Homo_sapiens.txt.gz", sep="" )
  human$read$gene.coords.file <- paste( human$read$data.dir, "/human.ptt.gz", sep="" )
  human$read$pd.interaction.sif <- NULL
  human$read$goa.file <- NULL
  human$read$swp.file <- NULL
  human$read$kegg.pathways.file <- NULL
  human$read$seed.file <- paste( human$read$data.dir, "seed_set_050408.txt", sep="" )
  #human$read$seed.file <- paste( human$read$data.dir, "LinSeed.txt", sep="" )
  human$read$sample.seed <- T
  
  human$read$predictome.code <- "human"              ## Code for species
  
####################### SMD DATA FILES ############################

  human$read$data.col <- c( "LOG_RAT2N_MEAN", "Log.base2..of.R.G.Normalized.Ratio..Mean." )
  human$read$gene.col <- c( "Name", "NAME" )
  human$read$desc.name <- "DESCRIPTION" # Not used yet
  human$read$short.name <- "GENE.NAME" # Not used yet

  human$read$valid.gene.regexp <- "^GBAA.+$" # Use this to rule out bad spots in SMD data
  human$read$invalid.condition.regexp = c( "Category=Strain comparison" )

####################### INITIALIZATION ###########################
  
  ##human$read$nColGroups <- human$read$kColGroups <- 0
  ##human$read$seedMode <- "full" ##"rnd"

  human$read$stdRatios <- TRUE ## FALSE
  human$read$ratios.thresh <- 0.25 ##1.0
  human$read$ratios.n.thresh <- 5
  human$read$ratios.max.nas.perc <- (1/3)
  
####################### MOTIF FINDING #############################

  human$read$tmp.prefix <- paste( human$organism, "_Motif_tmp", sep="" )
  ##human$iter <- list()
#  human$iter$motif.scaling <- c( 0, seq( 0, 0.75, length=30 ), seq( 0.75, 0.25, length=50 ) )
#  names( human$iter$motif.scaling ) <- as.character( c( 1, 5:34, 35:84 ) )
  #human$iter$motif.scaling <- c( 0, seq( 0, 0.75, length=30 ), seq( 0.75, 0.50, length=50 ) )
  
#  human$iter$n.motifs <- unlist( list( "1"=1, "25"=2, "40"=3 ) ) ##"35"=2, "50"=3 ) )
  #human$iter$n.motifs <- unlist( list( "1"=1, "25"=2, "60"=3 ) ) ##"35"=2, "50"=3 ) )
  
  #human$iter$max.motif.width <- 25
  #human$iter$max.motif.width <- 20
  
####################### NETWORKS ##################################

##  human$net.max.weights <- unlist( list(operons=2,
##                                        met=0.8,
##                                        prolinks_RS=0,
##                                        prolinks_PP=0.5,
##                                        prolinks_GN=0,
##                                        prolinks_GC=0,
##                                        COG_code=0,
##                                        cond_sims=0,
##                                        predictome_CP=0,
##                                        predictome_GF=0,
##                                        predictome_PP=0.1 ) )

#  human$net.max.weights <- unlist( list(operons=2,
##                                         met=0.8,
##                                         prolinks_RS=0.3,
##                                         prolinks_PP=0.4,
##                                         prolinks_GN=0.6,
##                                         prolinks_GC=0.4,
##                                         COG_code=1,
##                                         cond_sims=1,
##                                         predictome_CP=0,
##                                         predictome_GF=0,
##                                         predictome_PP=0.1 ) )
  
  ##human$net.weights <- c( 0.06, 0.04, 0.02, 0.06, 0.04, 0.0, 0.0 )
  ##names( human$net.weights ) <- c( "met",
                                     ##"prolinks_RS",
                                     ##"prolinks_PP",
                                     ##"prolinks_GN",
                                     ##"prolinks_GC",
                                     ##"COG_code",
                                     ##"cond_sims" )

  ##human$net.weights <- human$net.weights * 0

  #human$iter$net.scaling <- c( seq( 0.1, 0.2, length=5 ), seq( 0.2, 0.02, length=50 ) )
  #human$iter$net.scaling <- c( seq( 0.1, 0.2, length=5 ), seq( 0.2, 0.45, length=60 ) )
  
####################### KILL PARAMS ###############################

  ##human$kill.count <- 1 # Kill this many clusters each time

  invisible( human )
}

humanParams()
