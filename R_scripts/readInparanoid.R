try( detach( read.inparanoid ), silent=T )
if ( exists( "read.inparanoid" ) ) rm( read.inparanoid )

read.inparanoid <- list( 

readInparanoid = function( inp.res.file, organism1, organism2 ) {

  lines <- scan( inp.res.file, what='character', sep="\n" )
  separators <- grep( "____", lines )

  records <- list()
  
  for ( i in 1:length( separators ) ) {
    first.line <- separators[ i ] + 3
    last.line <- ifelse( i == length( separators ),
                         length( lines ),
                         ( separators[ i+1 ] - 1 )
                        )
    records[[ i ]] <- character()
    for ( j in first.line:last.line )
      records[[i]] <- c( records[[i]], lines[j] )
  }

  # return (records  )
  parse.records <- function ( record,
                              organism1=organism1,
                              organism2=organism2 ) {

    splitted <- strsplit( record, "\t" )
    org1 <- sapply( splitted, "[[", 1 )
    org1 <- org1[ grep( "\\w+", org1, perl=T ) ]
    if ( length( grep( "\\|", org1 ) ) > 0 ) {
      ## backwards compatibility with the output from inparanoid v.2 - which could handle
      ##  RSAT's full FASTA headers.  We now just use the gi #'s b/c of a limitation with
      ##  inparanoid v.3
      org1 <- sapply( strsplit( org1, "\\|" ), "[[", 2 )
    }

    org2 <- sapply( splitted, "[[", 4 )
    org2 <- org2[ grep( "\\w+", org2, perl=T ) ]
    if ( length( grep( "\\|", org2 ) ) > 0 ) {
      org2 <- sapply( strsplit( org2, "\\|" ), "[[", 2 )
    }


    # now create the pairs
    pairs <- data.frame( matrix( 0,
                                 nr=1,
                                 nc=2,
                                 dim=list( "", c( organism1, organism2 ) )
                                )
                        )
    for ( orgi in org1 ) {
      for ( orgj in org2 ) {
        new.pair <- as.integer( c( orgi, orgj ) )
        names( new.pair ) <- c( organism1, organism2 )
        pairs <- rbind( pairs, new.pair )
      }
    }

    pairs <- pairs[ -1, ]
    return( pairs )
    #  map the gi #'s to the gene_ids or gene.names??
    # return a list of pairs
    
  }


  
  # line below splits the pairs by their ortholog groups
  ret.list <- sapply( records,
                      parse.records,
                      organism1,
                      organism2,
                      simplify=F
                     )


  #  uncomment line below if we want to return a list of records
  

  #  these lines "flatten" the list of group data.frames into a single matrix
  #   though it does not identify which o'log group a pair belongs to
  temp <- unlist( sapply( records,
                          parse.records,
                          organism1,
                          organism2,
                          simplify=F
                         )
                 )
#  return( temp )
  #  this line just creates a int vector containing the group id's 
  #   w/1 for each member of the group, i.e. if group 1 has 4 pairs, this
  #   will have 1,1,1,1 (one for each pair in the group )
  group.ids <- unlist( sapply( 1:length( ret.list ),
                               function( x ) {
                                 rep( x, nrow( ret.list[[ x ]] ) )
                               }
                              )
                      )

  #  a new vector to contain temp, but with each pair id'd by it's o'log group
  retVal <- matrix( 0,
                    nr = length( group.ids ),
                    nc = 3,
                    dim = list( 1:length( group.ids ),
                                c( "group.id", organism1, organism2 )
                               )
                   )
  
  #  really F'd up code below, but basically temp is a named array, with
  #   the organisms as the roots of the names, i.e. bsubt, bsubt1, bsubt2
  #   so, we grep on these and put them into the right column of retVal
  retVal[ ,"group.id" ] <- group.ids
  retVal[ ,organism1 ] <- temp[ grep( organism1, names( temp ) ) ]
  retVal[ ,organism2 ] <- temp[ grep( organism2, names( temp ) ) ]  

  return( retVal )
},



pairs.pids.to.pairs.ids = function( pairs.pids,
                                    organisms
                                   ) {
  if ( class( pairs.pids ) == "list" )
    return( list.pairs.pids.to.pairs.ids( mycon=NULL,
                                          pairs.pids,
                                          organisms
                                         )
           )
  if ( class( pairs.pids ) == "matrix" )
    return( matrix.convert.pairs.pids( pairs.pids,
                                       organisms,
                                       new.id="id"
                                      )
           )

  stop( "incorrect data-type passed as pairs.pids\n")
  

},
                        


#  Beware of this fn - probably needs to be updated and is buggy
#  has not been smoke tested since we switched to matrix format
#  heck, it hasn't even been written yet!
list.pairs.pids.to.pairs.ids = function( mycon,
                                          pairs.pids,
                                          organisms
                                         ) {
#  TBD
},



##  new.id param is used to specify whether or not we want
##  to use id's or names (in case of using a a 'named' ratios table
matrix.convert.pairs.pids = function( pairs.pids,
                                      organisms,
                                      new.id="id" 
                                     ) {


  ## first filter pairs.pids to only include those we have in the db
  db.gene.pids <- list()
  for ( org in organisms ) {
    mycon <- db.open.con( org )

    if ( new.id == "id" )
      db.gene.pids[[ org ]] <- mysqlQuickSQL( mycon, "select gene_id,ncbi_pid from genes" )
    else if ( new.id == "name" )
      db.gene.pids[[ org ]] <- mysqlQuickSQL( mycon, "select gene_name,ncbi_pid from genes" )
    else
      stop ( paste( "invalid new.id mode in matrix.convert.pairs.pids, new.id =", new.id, "\n" ) )

    tmp.names <- db.gene.pids[[ org ]][ , "ncbi_pid" ]
    if ( new.id == "id" )
      db.gene.pids[[ org ]] <- as.numeric( db.gene.pids[[ org ]][, 1] )
    else
      db.gene.pids[[ org ]] <- db.gene.pids[[ org ]][, 1]
    names( db.gene.pids[[ org ]] ) <- tmp.names

    ## the pids in pairs.pids for a give organism
    pairs.pids.organism <- as.character( pairs.pids[ , org ] )

    pairs.pids <- pairs.pids[ pairs.pids.organism %in% names( db.gene.pids[[ org ]] ), ]
    dbDisconnect( mycon )
  }

  # now update the pids to the gene id's we've assigned
  pairs.ids <- pairs.pids
  for ( org in organisms ) {
    pairs.ids[ , org ] <- db.gene.pids[[ org ]][ as.character( pairs.ids[ , org ] ) ]
    if ( new.id == "id" )
      pairs.ids[ , org ] <- as.numeric( pairs.ids[ , org ] )
  }
  return( pairs.ids )
},



pairs.pids.to.pairs.names = function( mycon,
                                       pairs.pids,
                                       organisms
                                      ) {
  if ( class( pairs.pids ) == "list" )
    return( lists.pair.pids.to.pairs.names( mycon,
                                            pairs.pids,
                                            organisms
                                           )
           )
  if ( class( pairs.pids ) == "matrix" )
    return( matrix.convert.pairs.pids( pairs.pids,
                                       organisms,
				       new.id="name"
                                      )
           )

  stop( "inccorrect data-type passed as pairs.pids\n")
  

},


#  Beware of this fn - probably needs to be updated and is buggy
#  has not been smoke tested since we switched to matrix format
list.pairs.pids.to.pairs.names = function( pairs.pids,
                                           organisms
                                          ) {
  parse.olog.groups <- function( group ) {

    result <- data.frame()

    for ( org in organisms ) {
      mycon <- db.open.con( org )

      gene.list <- character()
      
      for ( i in 1:nrow( group ) ) {
        gene.names <- mysqlQuickSQL( mycon,
                                     paste( "select gene_name from genes",
                                            "where ncbi_pid=",
                                            group[ i, org ]
                                           )
                                    )
      if ( nrow( gene.names ) > 0 ) 
        gene.list <- c( gene.list, gene.names[1,1] )
      else
        gene.list <- c( gene.list, "" )

      }

      if ( nrow( result ) == 0 )
        result <- rbind( result, gene.list )
      else
        result <- cbind( t( result ), gene.list )

      dbDisconnect( mycon )
    }

    result <- as.matrix( result )
    colnames( result ) <- organisms
    rownames( result ) <- 1:nrow( result )
    result <- data.frame( result )
    
    return( result )
  }

  return( sapply( pairs.pids,
                  parse.olog.groups,
                  simplify=F
                 )
         )
}

)

attach( read.inparanoid )
