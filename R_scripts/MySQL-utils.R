## Nov. 2007 SM
## Multi-species database functions

# Parameter "named": true if the ratios table has named rows and columns, false otherwise.
# Setting default to true for now, will be false later to optimize for speed.

# Ratios matrix passed in instead of marray file name.
# TODO: how does R pass parameters?  By value or by reference?
#   Ans from PHW: both.  depends on the size of var being passed, i.e
#           small == by value
#           large == by reference
#        I'm not sure what is the threshold by which small v. large
#        is determined

if ( ! length( grep( "RMySQL", search() ) ) > 0 )
  require( RMySQL )

try( detach( "mysql.utils" ), silent=T )
if ( exists( "mysql.utils" ) ) rm( mysql.utils )

mysql.utils <- list( 


# use this to connect to the mysql svr and database
db.open.con = function( organism=get.global( "organism" ) ) {
  mycon <- dbConnect( MySQL(),
                      group = organism
                     )
  return( mycon )
},
  


#  use to connect to the mysql svr w/o opening a DB
connect.2.mysql.svr = function(){

  return( dbConnect( MySQL(),
                     group = "general"
                    )
         )
},



# verifies whether a DB is on the mysql server
#  will create a connection if necessary and if so, will remove it
does.db.exist = function( mycon=NULL,
                          organism=get.global( "organism" )
                         ) {

  #  set up a connection if necessary, and make sure to clean up
  #   if we have to create one
  disconnect <- F
  if ( is.null( mycon ) ) {
    mycon <- connect.2.mysql.svr()
    disconnect <- T
  }

  rs <- dbSendQuery( mycon, "show databases" )
  data <- fetch( rs )

  if ( disconnect )
    dbDisconnect( mycon )

  return( organism %in% data[,1] )
},



# organism is the organism who's db we're creating
create.organism.db = function( mycon=NULL,
                               organism=get.global( "organism" ) ) {
  
  #  set up a connection if necessary, and make sure to clean up
  #   if we have to create one
  disconnect <- F
  if ( is.null( mycon ) ) {
    mycon <- connect.2.mysql.svr()
    disconnect <- T
  }

  
  #  OK, we've opened a connection to the mysql daemon, now try creating
  #  the organism DB
  tryRes <- try( rs <- mysqlExecStatement( mycon,
                                           paste( "create database",  organism )
                                          ),
                 silent=T
                )
    if ( class( tryRes ) == "try-error" ) {
      cat.new( "can't create database",
               organim,
               "Got the following err msg",
               "\n",
               tryRes[1],
               "\n"
              )
      stop( paste( "can't create database",
                   organim,
                   "Got the following err msg",
                   "\n",
                   tryRes[1],
                   "\n"
                  )
           )
    }

  if ( disconnect )
    dbDisconnect( mycon )

  return( mycon )
},
                    

  
# organism is the organism who's db we're creating
drop.organism.db = function( mycon=NULL,
                             organism=get.global( "organism" ) ) {

  #  set up a connection if necessary, and make sure to clean up
  #   if we have to create one
  disconnect <- F
  if ( is.null( mycon ) ) {
    mycon <- connect.2.mysql.svr()
    disconnect <- T
  }
  else {
    if ( dbGetInfo( mycon )$dbname == organism ) {
      dbDisconnect( mycon )
      mycon <- connect.2.mysql.svr()
    }
  }

  
  #  OK, we've opened a connection to the mysql daemon, now try creating
  #  the organism DB
  tryRes <- try( rs <- mysqlExecStatement( mycon,
                                           paste( "drop database",  organism )
                                          ),
                 silent=T
                )
    if ( class( tryRes ) == "try-error" ) {
      cat.new( "can't drop database",
               organim,
               "Got the following err msg",
               "\n",
               tryRes[1],
               "\n"
              )
      stop( paste( "can't drop database",
                   organim,
                   "Got the following err msg",
                   "\n",
                   tryRes[1],
                   "\n"
                  )
           )
    }

  if ( disconnect )
    dbDisconnect( mycon )
  return( mycon )
},



# organism is the organism who's db we're switching to use
use.organism.db = function( mycon=NULL,
                            organism=get.global( "organism" ) ) {

  #  set up a connection if necessary
  if ( ! is.null( mycon ) ) {
    dbDisconnect( mycon )
  }

  return( db.open.con( organism ) )
},

                    

get.gene.ids.db = function( gene.names,
                            organism=get.global( "organism" )
                            ) {
  mycon <- db.open.con( organism )
  rs <- dbSendQuery( mycon,
                     paste( "select gene_id from genes nolock where gene_name IN (\"",
                            paste( gene.names, collapse="\",\"" ),
                            "\")",
                            sep=""
                           )
                    )
  retVal <- as.integer( fetch( rs, n=-1 )[[ "gene_id" ]] )
  dbDisconnect( mycon )
  return( retVal )
},
                     

get.gene.names.db = function( gene.ids,
                              organism=get.global( "organism" )
                            ) {
  mycon <- db.open.con( organism )
  rs <- dbSendQuery( mycon,
                     paste( "select gene_name from genes nolock where gene_id IN (",
                            paste( gene.ids, collapse="," ),
                            ")",
                            sep=""
                           )
                    )
  retVal <- as.character( fetch( rs, n=-1 )[[ "gene_name" ]] )
  dbDisconnect( mycon )
  return( retVal )
},



get.prot.names.db = function( gene.ids=NULL,
                              gene.names=NULL,
                              organism=get.global( "organism" )
                            ) {
  mycon <- db.open.con( organism )

  if ( ! is.null( gene.ids ) )
    rs <- dbSendQuery( mycon,
                       paste( "select prot_name from genes nolock where gene_id IN (",
                              paste( gene.ids, collapse="," ),
                              ")",
                              sep=""
                             )
                      )
  else {
    if ( ! is.null( gene.names ) )
      rs <- dbSendQuery( mycon,
                         paste( "select prot_name from genes nolock where gene_name IN (\"",
                                paste( gene.names, collapse="\",\"" ),
                                "\")",
                                sep=""
                               )
                        )
    else
      stop( "must specify either gene.names or gene.ids to get.prot.name.db\n" )
  }
  
  retVal <- as.character( fetch( rs, n=-1 )[[ "prot_name" ]] )
  dbDisconnect( mycon )
  return( retVal )
},



get.num.genes.db = function( organism=get.global( "organism" ) ) {
  mycon <- db.open.con( organism )
  retVal <- fetch( dbSendQuery( mycon,
                                "select max( gene_id ) from genes nolock"
                               )
                  )[ 1, 1 ]
  dbDisconnect( mycon )
  return( retVal )
},
                               


get.num.conds.db = function( organism=get.global( "organism" ) ) {
  mycon <- db.open.con( organism )
  retVal <- fetch( dbSendQuery( mycon,
                                "select max( cond_id ) from conditions nolock"
                               )
                  )[ 1, 1 ]
  dbDisconnect( mycon )
  return( retVal )
},



gene.code.from.db = function ( genes,
                               organism=get.global( "organism" ) ) {
  mycon <- db.open.con( organism )

  stmt <- "select gene_code from genes nolock where gene_id in("
  stmt <- paste( stmt,
                 paste( genes, collapse="," ),
                 ")",
                 sep="",
                 collapse=""
                )

  retVal <- fetch( dbSendQuery( mycon,
                                stmt
                               )
                  )$gene_code
  dbDisconnect( mycon )
  return( as.character( retVal ) )
  
},




gene.func.from.db = function ( genes, 
                               organism=get.global( "organism" ) ) {
  mycon <- db.open.con( organism )

  stmt <- "select gene_func from genes nolock where gene_id in("
  stmt <- paste( stmt,
                 paste( genes, collapse="," ),
                 ")",
                 sep="",
                 collapse=""
                )

  retVal <- fetch( dbSendQuery( mycon,
                                stmt
                               ),
                   n=-1
                  )$gene_func
  dbDisconnect( mycon )
  return( as.character( retVal ) )
  
},
                    
                    

setup.DBs.for.cMonkey = function( organisms=get.global( "organisms" ),
                                  overwrite=T
                                  ) {
  if ( ! length( organisms ) > 0 ) organisms <<- get.organisms()


  ##  SIGH...I LOATHE having to use a global var, shared.networks, to store the 
  ##     network data, but it is fastest way
  if ( "network" %in% data.types.used )
    shared.networks <- list()

  for ( org in organisms ) {
#  for ( org in c( "bsubt" ) ) {
    
    ## check to see if the organisms's db already exists
    ##  and handle it if it does
    mycon <- connect.2.mysql.svr()
    db.exists.flag <- does.db.exist( mycon,
                                     org
                                    )

    if ( db.exists.flag ) {
      if ( overwrite )
        mycon <- drop.organism.db( mycon, org )
      else
        next
    }
    
    mycon <- create.organism.db( mycon, org )
    dbDisconnect( mycon )


    ## If we've gotten to here, the database exists, but the tables 
    ##  aren't defined yet


    ## attach the correct, organism-specific params for data reading
    attach( params$read[[ org ]] )
    ##browser()
    fname <- paste( organism, "org.data", "RData", sep="." )
    if ( file.exists( fname ) ) {
      cat.new( 'loading org.data from', fname, '\n' )
      load( fname )
    }
    else {
      org.data <- readAllData( )
      save( file=fname, org.data )
    }

    if ( "expression" %in% data.types.used ) {
      cat.new( "writing ratios for", organism, "\n" )
      ratios.to.db( organism=org,
                    ratios=org.data$ratios,
                    gene.coords=org.data$gene.coords,
                    maxRowVar=org.data$maxRowVar
                   )
    }

    if ( "seq.motif" %in% data.types.used || "network" %in% data.types.used ) {
      cat.new( "writing opUpstream for", organism, "\n" )
      ##this will be a lookup table for us
      gene.name.to.id.map <- 1:get.num.genes.db( org )
      names( gene.name.to.id.map ) <- get.gene.names.db( gene.ids=gene.name.to.id.map,
                                                         organism=org )  

      if ( "seq.motif" %in% data.types.used ) {

        names( org.data$opUpstream ) <- gene.name.to.id.map[ names( org.data$opUpstream ) ]
        ##  Now throw it into the SQL db
        opUpstream.to.db( opUpstream=org.data$opUpstream,
                          organism=org )

      }

      if ( "network" %in% data.types.used ) {
        cat.new( "writing networks for", organism, "\n" )

        for ( net in names( org.data$networks$n.edges ) ) {

          if ( net.max.weights[[ net ]] == 0 || is.null( org.data$networks[[ net ]] ) )
            next

          cat( org, ": net=", net,"\n", sep="" )
          net.to.db( net.matrix=org.data$networks[[ net ]],
                     name=net,
                     gene.name.to.id.map=gene.name.to.id.map,
                     organism=org
                    )
          names( org.data$networks$adj.lists[[ net ]] ) <- NULL
          org.data$networks$adj.lists[[ net ]] <- sapply( org.data$networks$adj.lists[[ net ]],
                                                          function( adj.list )
                                                            as.integer( gene.name.to.id.map[ adj.list ] ),
                                                          simplify=F,
                                                          USE.NAMES=T
                                                         )
          org.data$networks[[ net ]] <- matrix( gene.name.to.id.map[ org.data$networks[[ net ]] ],
                                                nc=2 )
        }

        #save( file=paste( net,'.import.shared.nets.dbg.rda', sep=""), list=ls() )
        net.names.to.db( n.edges=org.data$networks$n.edges,
                         net.max.weights,
                         organism=org )

        ##  NOTE, shared.networks declared above the outer for-loop (for ( org in organisms ) )
        shared.networks[[ org ]] <- org.data$networks        

      }
    }

    ## last, but not least, detach the organism-specific params for data reading
    detach( "params$read[[org]]")
  }

  if ( "networks" %in% data.types.used ) {
    shared.nets.fname <- paste( output.dir, "shared.networks.RData", sep=""  )
    save( file=shared.nets.fname, shared.networks )
  }

  dbDisconnect( mycon )
},


                    
net.names.to.db = function( n.edges,
                            net.max.weights,
                            organism=get.global( "organism" )
                           ) {

  mycon <- db.open.con( organism )
  net.names <- names( n.edges )
  net.names <- net.names[ net.names %in% names( net.max.weights[ net.max.weights > 0 ] ) ]
  names.df <- as.data.frame( cbind( as.integer( 1:length( net.names ) ),
                                    as.character( net.names ),
                                    as.numeric( n.edges[ net.names ] )
                                   )
                            )
  names( names.df ) <- c( "net_id", "net_name", "n_edges" )

  d <- dbWriteTable( mycon,
                     name="network_names",
                     names.df,
                     overwrite=T,
                     append=F,
                     row.names=F,
                     field.types=list( net_id="int",
                                       net_name="text",
                                       n_edges="double" )
                    )
  dbDisconnect( mycon )
},



net.names.from.db = function( organism=get.global( "organism" ) ) {

  mycon <- db.open.con( organism )
  
  ##net.names <- dbReadTable( mycon, "network_names" )
  rs <- dbSendQuery( mycon, "select * from network_names nolock" )
  net.names <- fetch( rs, n=-1 )
  net.names <- as.character( net.names$net_name )

  dbDisconnect( mycon )
  return( net.names )
},



                    
net.names.tbl.from.db = function( organism=get.global( "organism" ) ) {

  mycon <- db.open.con( organism )
  ##net.names <- dbReadTable( mycon, "network_names" )
  rs <- dbSendQuery( mycon, "select * from network_names nolock" )
  net.names <- fetch( rs, n=-1 )
  dbDisconnect( mycon )
  return( net.names )
},



                    
net.n.edges.from.db = function( net,
                                organism=get.global( "organism" ) ) {

  mycon <- db.open.con( organism )
  n.edges <- mysqlQuickSQL( mycon, paste( "select n_edges from network_names nolock where net_name ='", net,"'", sep="" ) )$n_edges
  dbDisconnect( mycon )
  return( n.edges )
},
                    


                    
net.from.db = function( net,
                        organism=get.global( "organism" ) ) {

  mycon <- db.open.con( organism )
  
  ##net <- as.matrix( dbReadTable( mycon, net ) )
  rs <- dbSendQuery( mycon, paste( "select * from", net, "nolock" ) )
  net <- fetch( rs, n=-1 )

  dbDisconnect( mycon )
  return( as.matrix( net ) )
},

                    

##  unfinished - don't use.  requires 2x as much space as net.matrix struct
##    and doesn't appear to offer much for an adj.list.from.db fn other
##    than halving the number of sql queries
adj.list.to.db = function( adj.list,
                           organism=get.global( "organism" ) ) {
  mycon <- db.open.con( organism )

  adj.list <- unlist( adj.list )
  adj.list.ids <- unlist( sapply( 1:length( adj.list ),
                                  function( gene ) rep( gene, length( adj.list[[ gene ]] ) )
                                 )
                         )
  to.db <- as.data.frame( cbind( adj.list.ids, adj.list ) )
  names( to.db ) <- c( "gene_from", "gene_to" )

  ## write to.db to db here
  
  dbDisconnect( mycon )  
},


                    
##  assumes network as a matrix
adj.list.from.db.mat = function( net,
                                 gene,
                                 organism=get.global( "organism" ) ) {
  mycon <- db.open.con( organism )

  stmt <- paste( "select gene_to from", net, "nolock where gene_from =", gene )
  stmt2 <- paste( "select gene_from from", net, "nolock where gene_to =", gene )

  #cat( stmt, "\n" )
  #cat( stmt2, "\n" )  
  
  retVal <- unique( as.numeric( c( mysqlQuickSQL( mycon, stmt )$gene_to,
                                   mysqlQuickSQL( mycon, stmt2 )$gene_from
                                  )
                               )
                   )
  dbDisconnect( mycon )
  return( retVal )
},

                    
                    
net.to.db = function( net.matrix,
                      name,
                      gene.name.to.id.map,
                      organism=get.global( "organism" )
                     ) {
  if ( class( net.matrix ) != "matrix" )
    stop( "invalid net.matrix passed to net.to.db for", name, "db - not a matrix\n" )
  if ( ncol( net.matrix ) != 2 )
    stop( "invalid net.matrix passed to net.to.db for", name, "db - num cols != 2\n" )



  ##  since only 2 cols in matrix, do it brute force
  net.matrix[,1] <- gene.name.to.id.map[ net.matrix[,1] ]
  net.matrix[,2] <- gene.name.to.id.map[ net.matrix[,2] ]
  ## not necessary for cMonkey, but is for MySQL
  colnames( net.matrix ) <- c( "gene_from", "gene_to" )
  
  mycon <- db.open.con( organism )
  dbWriteTable( mycon,
                name=make.db.names( MySQL(), name ),
                as.data.frame( net.matrix ),
                overwrite = T,
                append = F,
                row.names  = F,
                field.types=list( gene_from="int",
                                  gene_to="int"
                                 )
               )
  
  dbDisconnect( mycon )
  
},
  

opUpstream.to.db = function( opUpstream,
                             organism=get.global( "organism" )
                           ) {

  opUpstream.table <- cbind( names( opUpstream ),
                             opUpstream
                            )

  colnames( opUpstream.table ) <- c( "gene_id", "seq" )
  mycon <- db.open.con( organism )

  d <- dbWriteTable( mycon,
                     "opUpstream",
                     as.data.frame( opUpstream.table ),
                     overwrite = T,
                     append = F,
                     row.names  = F,
                     field.types=list( gene_id="int",
                                       seq="text"
                                      )
                    )

  dbDisconnect( mycon )
},



opUpstream.from.db = function( organism=get.global( "organism" ),
                               genes=NULL ){
  
  mycon <- db.open.con( organism )
  opUp.exists <- dbExistsTable( mycon,
                                "opUpstream" )

  if ( !opUp.exists ){
    dbDisconnect( mycon )
    stop("Error: opUpstream DB does not exist on MySQL svr\n" )
  }

  if ( is.null( genes ) ) {
    ##opTable <- dbReadTable( mycon, "opUpstream" )
    rs <- dbSendQuery( mycon, "select * from opUpstream nolock" )
    opTable <- fetch( rs, n=-1 )
  }
  else {
    stmt <- "select * from opUpstream nolock where gene_id in("
    stmt <- paste( stmt,
                   paste( genes, collapse="," ),
                   ")",
                   sep="",
                   collapse=""
                  )

    rs <- dbSendQuery( mycon, stmt )
    opTable <- fetch( rs, n=-1 )
  }

  dbDisconnect( mycon )
  retVal <- as.character( opTable[ , "seq" ] )
  names( retVal ) <- opTable[ , "gene_id" ]

  return( retVal )

},



pairs.to.db = function( pairs,
                        organisms=get.global( "organisms" )  ) {

  mycon <- connect.2.mysql.svr()
  tblname <- paste( organisms, collapse="_")
  pairs.exist <- does.db.exist( mycon,
                                "pairs"
                               )

  if ( !pairs.exist )
    create.organism.db( mycon,
                        organism="pairs"
                       )

  mycon <- use.organism.db( mycon,
                            organism="pairs"
                           )
  
  d <- dbWriteTable( mycon,
                     tblname,
                     as.data.frame( pairs ),
                     overwrite = T,
                     append = F,
                     row.names  = F
                    )

  dbDisconnect( mycon )
},


pairs.from.db = function( mycon=NULL,
                          organisms=get.global( "organisms" ) ) {
  disconnect <- F
  #  set up the connection
  if ( is.null( mycon ) ) {
    disconnect=T
    mycon <- connect.2.mysql.svr()
  }


  # check that the pairs DB exists
  pairs.exist <- does.db.exist( mycon,
                                "pairs"
                               )
  
  if ( !pairs.exist ){
    dbDisconnect( mycon )
    stop("Error: pairs DB does not exist on MySQL svr\n" )
  }

  #  connect to the pairs dB
  mycon <- use.organism.db( mycon,
                            organism="pairs"
                           )

  ##  verify that the pairs table exists in the pairs DB
  tblname <- paste( organisms, collapse="_")  
  if ( !dbExistsTable( mycon, tblname ) ) {
    stop("Error: cannot find", tblename, "table in pairs DB\n" )
  }

  retVal <- dbReadTable( mycon, tblname )

  if ( disconnect )
    dbDisconnect( mycon )
  
  return( retVal )

},


#  fn to add the ratios to the DB.
#   Note, the fn doesn't do any connection/table/db checking as
#   this is done in setup.DBs.for.cMonkey
ratios.to.db = function( organism,
                         ratios,
                         gene.coords,
                         maxRowVar
                        ) {
  
  mycon <- db.open.con( organism )
  
  ## genes table
  gene.table <- cbind( c( 1:nrow( ratios ) ),                       # gene_id's
                       rownames( ratios ),                          # gene_name's
                       gene.coords$gene.name[ rownames( ratios ) ], # prot_name's (i.e. dnaA)
                       gene.coords$gene.func[ rownames( ratios ) ], # gene_func's
                       gene.coords$gene.code[ rownames( ratios ) ], # gene_code's
                       gene.coords$gene.pid[ rownames( ratios ) ]   # ncbi_pid's
                      )
  colnames( gene.table ) <- c( "gene_id", 
                               "gene_name", 
			       "prot_name", 
			       "gene_func", 
			       "gene_code", 
			       "ncbi_pid" 
                    	      )
  d <- dbWriteTable( mycon,
                     "genes",
                     as.data.frame( gene.table ),
                     overwrite = T,
                     append = F,
                     row.names  = F,
                     field.types=list( gene_id="int",
                                       gene_name="text",
                                       prot_name="text",
                                       gene_func="text",
                                       gene_code="text",
                                       ncbi_pid="int"
                                     )
                    
                    )

  # conditions table
  col2 <- colnames( ratios )
  col1 <- c( 1:length( col2 ) )
  cond.table <- cbind( col1, col2 )
  colnames( cond.table ) <- c( "cond_id", "cond_name" )
  d <- dbWriteTable( mycon,
                     "conditions",
                     as.data.frame( cond.table ),
                     overwrite = T,
                     append = F,
                     row.names = F,
                     field.types=list( cond_id="int",
                                       cond_name="text"
                                     )

                    )

  # maxRowVar "table" - just has a single entry in it
  if ( class( maxRowVar ) != "data.frame" ) {
    maxRowVar <- data.frame( maxRowVar )
    colnames( maxRowVar ) <- c( "maxRowVar" )
  }
  d <- dbWriteTable( mycon,
                     "maxRowVar",
                     maxRowVar,
                     overwrite=T,
                     append=F,
                     row.names=F,
                     field.types=list( maxRowVar="double" )
                    )

  
  tablename <- "ratios_unnamed"
  field.types=list( gene_id="int",
                    cond_id="int",
                    value="double" )
  apnd <- F
  for ( i in 0:( ncol( ratios ) %/% 100 ) ) {

    if ( ( (i+1) * 100 ) < ncol( ratios ) )
      num <- 100
    else
      num <- ncol( ratios ) - ( i*100 )

    d <- dbWriteTable( mycon,
                       tablename,
                       as.data.frame( cbind( rep( seq( nrow( ratios ) ),
                                                  num
                                                 ),
                                             sort( rep( seq( from=(i * 100  + 1 ),
                                                             length=num
                                                            ),
                                                        nrow( ratios )
                                                       )
                                                  ),
                                             as.numeric( ratios[ , seq( from=(i * 100  + 1 ),
                                                                        length=num
                                                                       )
                                                                ]
                                                        )
                                            )
                                     ),
                       overwrite = !apnd,
                       append = apnd,
                       row.names=F,
                       field.types=field.types
                      )
    apnd <- T
  }
  
  dbDisconnect( mycon )
},


ratios.from.db = function( mycon ) {
                          
  tablename <- "ratios_unnamed"

  if ( !dbExistsTable( mycon, tablename ) ||
       !dbExistsTable( mycon, "genes" ) ||
       !dbExistsTable( mycon, "conditions" ) ||
       !dbExistsTable( mycon, "maxRowVar" )
      ) {
    #TODO: exit on failure?
    stop("Error: cannot find all three necessary tables in DB for expression ratios\n")
  }

  ratios.stacked <- dbReadTable( mycon, tablename )
  ratios.genes <- dbReadTable( mycon, "genes" )
  ratios.conds <- dbReadTable( mycon, "conditions" )
  ratios.maxRowVar <- dbReadTable( mycon, "maxRowVar" )[1,1]

  ratios <- matrix( as.numeric( ratios.stacked$value ),
                    nr = nrow( ratios.genes ),
                    nc = nrow( ratios.conds )
                   )
  
  invisible( list ( ratios = ratios,
                    ratios.genes = ratios.genes,
                    ratios.conds = ratios.conds,
                    ratios.maxRowVar = ratios.maxRowVar
                   )
            )
},

expr.mat.from.db = function( organism ) {

  tablename <- "ratios_unnamed"

  mycon <- db.open.con( organism )
  if ( !dbExistsTable( mycon, tablename ) ||
       !dbExistsTable( mycon, "genes" ) ||
       !dbExistsTable( mycon, "conditions" ) ||
       !dbExistsTable( mycon, "maxRowVar" )
      ) {
    #TODO: exit on failure?
    stop("Error: cannot find all three necessary tables in DB for expression ratios\n")
  }

  ##ratios.stacked <- dbReadTable( mycon, tablename )
  rs <- dbSendQuery( mycon, paste( "select * from", tablename, "nolock" ) )
  ratios.stacked <- fetch( rs, n=-1 )
  dbDisconnect( mycon )

  ratios <- matrix( as.numeric( ratios.stacked$value ),
                    nr = get.num.genes.db( organism ),
                    nc = get.num.conds.db( organism ),
                   )

  invisible( ratios )
},

                    

maxRowVar.from.db = function( organism="bsubt" ) {
  mycon <- db.open.con( organism )
  retVal <- dbReadTable( mycon, "maxRowVar" )[1,1]
  dbDisconnect( mycon )
  return( retVal )
},

                    

## Input: list of genes, list of conditions
## Output: matrix with genes as rownames, conditions as col names, expressions as values
## TODO: Default organism is the current one -- does this make sense in mult-species?
get.biclust = function( bicl.genes,
                         bicl.conds,
                         organism=NULL
                        )
{
  if ( is.null( organism ) )
    stop( "organism", organism, "is null in get.biclust\n" )

  if ( exists( "ratios" ) && organism %in% names( ratios ) )
    return( ratios[[ organism ]][ bicl.genes, bicl.conds, drop=FALSE ] )

  
  mycon <- db.open.con( organism )

  ##  get the list of gene_id/names dept on structure passed in
  ##  we check if it's a dataframe, and assume it's a vector if not
  if ( class( bicl.genes ) == "data.frame" ) {
    genes <- bicl.genes[[ "gene_id" ]]
  }
  else
    genes <- bicl.genes


  #  get the list of cond_id/names dept on structure passed in
  #  we check if it's a dataframe, and assume it's a vector if not
  if ( class( bicl.conds ) == "data.frame" ) {
    conds <- bicl.conds[[ "cond_id" ]]
  }
  else
    conds <- bicl.conds

  
  stmt <-  paste( "SELECT *",
                  "FROM ratios_unnamed",
                  "NOLOCK WHERE gene_id IN (",
                  paste( genes,
                         sep="",
                         collapse=","
                        ),
                  ") AND cond_id IN(",
                  paste( conds,
                         sep="",
                         collapse=","
                        ),
                  ")"#, 
                  #" order by cond_id, gene_id"
                 )

  #return( stmt )
  res <- dbSendQuery( mycon,
                      statement = stmt
                     )
  tmp <- fetch(res, n = -1)
  tmp <- as.numeric( tmp[ order( tmp$cond_id, tmp$gene_id ), ]$value )
  bicl.table <- matrix( as.numeric( tmp ),
                        nrow=length( genes ),
                        ncol=length( conds )
                       )
  

  ### Cleanup ###
  dbDisconnect( mycon )

  return( bicl.table )
},



# Output: matrix with genes as rownames, conditions as col names, expressions as values
# TODO: Default organism is the current one -- does this make sense in mult-species?
get.rowslice = function( bicl.genes,
                          organism = "bsubt"
                         )
{


  if ( exists( "ratios" ) && organism %in% names( ratios ) )
    return( ratios[[ organism ]][ bicl.genes, ] )

  
  mycon <- db.open.con( organism )
  
  #  get the list of gene_id/names dept on structure passed in
  #  we check if it's a dataframe, and assume it's a vector if not
  if ( class( bicl.genes ) == "data.frame" ) {
    genes <- bicl.genes[[ "gene_id" ]]
  }
  else
    genes <- bicl.genes

#  return( genes )

  stmt <- "select * from ratios_unnamed nolock where gene_id in("
  stmt <- paste( stmt,
                 paste( genes, collapse="," ),
                 ")",
                 " order by cond_id, gene_id",
                 sep="",
                 collapse=""
                )


  rs <- dbSendQuery( mycon, stmt )
  bicl.table <- matrix( as.numeric( fetch( rs,
                                           n=-1
                                          )$value
                                   ),
                        nr=length( genes )#,
                        #byrow=T
                       )
                  

  dbDisconnect( mycon )
  return( bicl.table )
},



# Output: matrix with genes as rownames, conditions as col names, expressions as values
# TODO: Default organism is the current one -- does this make sense in mult-species?
get.colslice = function( bicl.conds,
                          organism = get.global( "organism" )
                         )
{

  if ( exists( "ratios" ) && organism %in% names( ratios ) )
    return( ratios[[ organism ]][ , bicl.conds ] )
  

  
  #  get the list of cond_id/names dept on structure passed in
  #  we check if it's a dataframe, and assume it's a vector if not
  if ( class( bicl.conds ) == "data.frame" ) {
    conds <- bicl.conds[[ "cond_id" ]]
  }
  else
    conds <- bicl.conds


  num.genes <- get.num.genes.db( organism )
  max.matrix.size <- 10^6

  if ( ( num.genes * length( conds ) ) > max.matrix.size ) {
    max.rows <- min( 3000, (max.matrix.size %/% get.num.conds.db( organism ) ) )

    bicl.table <- matrix( NA,
                          nr=num.genes,
                          nc=length( conds )
                         )
    
    for ( i in 0:(num.genes %/% max.rows ) ) {
      start.gene <- ( i*max.rows)+1
      cur.set <- start.gene:min( (start.gene+max.rows-1), num.genes )

      bicl.table[ cur.set, ] <- get.biclust( cur.set,
                                             conds,
                                             organism=organism )
      gc()
    }
  }
  else {
    mycon <- db.open.con( organism )
    stmt <- "select * from ratios_unnamed nolock where cond_id in ("
    stmt <- paste( stmt,
                   paste( bicl.conds, collapse="," ),
                   ")",
                   ##" order by cond_id, gene_id",  # do the sorting on the client side from now on
                   sep="",
                   collapse=""
                  )

    rs <- dbSendQuery( mycon, stmt )
    bicl.table <- fetch( rs, n=-1 )
    bicl.table <- bicl.table[ order( bicl.table$cond_id, bicl.table$gene_id ), ]
    bicl.table <- matrix( as.numeric( bicl.table$value ),
                          nc=length( conds )
                         )

    dbDisconnect( mycon )
  }
  return( bicl.table )
}

)

attach( mysql.utils )
