import os

#rest_urls = ["http://cicblade.dep.usal.es/psicquic-ws/webservices/current/search/",                  #0  APID 
#             "http://www.ebi.ac.uk/Tools/webservices/psicquic/chembl/webservices/current/search/",   #1  ChEMBL
#             "http://tyerslab.bio.ed.ac.uk:8080/psicquic-ws/webservices/current/search/",            #2  BioGrid
#             "http://imex.innatedb.com/webservices/current/search/",                                 #3  InnateDB
#             "http://imex.mbi.ucla.edu/psicquic-ws/webservices/current/search/",                     #4  DIP
#             "http://www.ebi.ac.uk/Tools/webservices/psicquic/intact/webservices/current/search/",   #5  IntAct
#             "http://matrixdb.ibcp.fr:8080/webservices/current/search/",                             #6  MatrixDB
#             "http://mint.bio.uniroma2.it/mint/psicquic/webservices/current/search/",                #7  Mint
#             "http://www.jcvi.org/mpidb/servlet/webservices/current/search/",                        #8  MPIDB
#             "http://reactome.oicr.on.ca:7080/psicquic-ws-reactome/webservices/current/search/",     #9  Reactome
#             "http://biotin.uio.no:8080/psicquic-ws/webservices/current/search/",                    #10 iRefIndex
#             "http://www.baderlab.org:8180/psicquic-ws-1.1.5/webservices/current/search/",           #11 BIND
#             "http://www.baderlab.org:8180/psicquic-interoporc-ws/webservices/current/search/",      #12 InteroPorc
#             "http://string.uzh.ch/psicquic/webservices/current/search/",                            #13 STRING
#             "http://reactome.oicr.on.ca:7080/psicquic-ws-reactome-fi/webservices/current/search/"]  #14 Reactome-Fi

rest_urls = ["http://cicblade.dep.usal.es/psicquic-ws/webservices/current/search/", "http://www.ebi.ac.uk/Tools/webservices/psicquic/chembl/webservices/current/search/", "http://tyerslab.bio.ed.ac.uk:8080/psicquic-ws/webservices/current/search/", "http://imex.innatedb.com/webservices/current/search/", "http://imex.mbi.ucla.edu/psicquic-ws/webservices/current/search/", "http://www.ebi.ac.uk/Tools/webservices/psicquic/intact/webservices/current/search/", "http://matrixdb.ibcp.fr:8080/webservices/current/search/", "http://mint.bio.uniroma2.it/mint/psicquic/webservices/current/search/", "http://www.jcvi.org/mpidb/servlet/webservices/current/search/", "http://reactome.oicr.on.ca:7080/psicquic-ws-reactome/webservices/current/search/", "http://biotin.uio.no:8080/psicquic-ws/webservices/current/search/", "http://www.baderlab.org:8180/psicquic-ws-1.1.5/webservices/current/search/", "http://www.baderlab.org:8180/psicquic-interoporc-ws/webservices/current/search/", "http://string.uzh.ch/psicquic/webservices/current/search/", "http://reactome.oicr.on.ca:7080/psicquic-ws-reactome-fi/webservices/current/search/"]
    ##  Note, APID is just an aggregator.  Most of the DB's it aggregates are alredy queried (BioGrid, DIP, Bind,
    ##  IntAct & MINT), but it also includes HPRD data, so we only filter for that.  In effect, APID is used to
    ##  access HPRD data.
tax = '10090'
             
for n, url in enumerate(rest_urls):
    full_url = "%squery/taxidA:%s%%20AND%%20taxidB:%s"%(url,tax,tax)
    cmd = "wget -o db_%02d.log -O db_%02d.tsv %s &"%(n,n,full_url)
    print cmd
    os.system(cmd)

